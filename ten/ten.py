#!/usr/bin/env python
# -*- coding: utf-8 -*-

"""
File: ten.py
Author: yourname
Email: yourname@email.com
Github: https://github.com/yourname
Description:
"""
Bots = {}
Outputs = {}

Watches = []

# logging
import logging
logging.basicConfig(format='%(asctime)s:%(levelname)s:%(name)s:%(message)s')
logger = logging.getLogger(__name__)
logger.setLevel(logging.INFO)

import itertools

import operator
import functools


class Thing(object):

    def __init__(self,
                 number,
                 items=None,
                 low=None,
                 high=None,
                 look=(),
                 started=False,
                 ):
        """TODO: Docstring for __init__.

        Parameters
        ----------
        number): : TODO

        Returns
        -------
        TODO

        """
        self.number = number
        self.items = items
        self.low = low
        self.high = high
        self.look = look
        self.transactions = []
        self.started = started

        if not self.items:
            self.items = []

    def __repr__(self):
        return '<%s:%s, items:%s, low:%s high:%s look:%s trans:%s>' % (self.__class__.__name__,
                                                                       self.number,
                                                                       repr(sorted(self.items, reverse=True)),
                                                                       self.low, self.high, self.look,
            len(self.transactions)
        )

    def receive(self, number, who):
        """ number is a mic
        who is the bot number we got it from
        """
        self.items.append(number)

        msg = 'received %s from %s' % (number, repr(who))
        self.transactions.append(msg)

        self.watch()

        if len(self.items) == 2:
            self.give_away()

    def give_away():
        """ pass for general items """

    def watch(self):
        if set(self.items) != set(self.look):
            msg = 'watch not found %s' % (self)
            logger.debug(msg)
            return

        if set(self.items) == set(self.look):
            msg = 'Watch found %s' % (self)
            logger.info(msg)

            Watches.append(self)


class Output(Thing):

    """ passive """


class Bot(Thing):

    def give_away(self):
        """ what = low|high """
        if self.started:
            self.items.sort()

            if self.low and self.high:
                for word, number in (self.low, self.high):
                    if word == 'bot':
                        other = Bots[number]

                    if word == 'output':
                        other = Outputs[number]

                    other.receive(self.items.pop(0), self)

                    msg = 'gave %s to %s' % (number, repr(other))
                    logger.debug(msg)
                    self.transactions.append(msg)


def parse(line, look):
    """ doc """
    if line.startswith('bot'):
        res = bot_line_parse(line, look)

    if line.startswith('value'):
        res = value_line_parse(line, look)


def advance():
    """ do the next move if anybody has 2 items """
    for num, bot in sorted(Bots.items()):
        if len(bot.items) == 2:
            if bot.high and bot.low and bot.started:
                bot.give_away()

    bar = 2 in [len(bot.items) for bot in Bots.values()]
    return bar


def add_bot_or_output(what, number, look):
    dispatch = {'bot': (Bots, Bot),
                'output': (Outputs, Output),
                }

    container, cls = dispatch.get(what)

    if number not in container:
        thing = cls(number=number, look=look)
        container[number] = thing

    if number in container:
        cls = container[number]

    return container, cls


def bot_line_parse(line, look):
    temp = line.split()

    bot_number = int(temp[1])
    container, cls = add_bot_or_output('bot', bot_number, look)

    _type = temp[5]
    low_bot = int(temp[6])
    cls.low = _type, low_bot
    add_bot_or_output(_type, low_bot, look)

    _type = temp[-2]
    high_bot = int(temp[-1])
    cls.high = _type, high_bot
    add_bot_or_output(_type, high_bot, look)

    return


def value_line_parse(line, look):
    temp = line.split()
    mic_number = int(temp[1])

    bot_number = int(temp[-1])
    add_bot_or_output('bot', bot_number, look)

    Bot = Bots[bot_number]
    Bot.receive(mic_number, who=None)

    return


def _test_part1():

    instructions = (
        'value 5 goes to bot 2',
        'bot 2 gives low to bot 1 and high to bot 0',
        'value 3 goes to bot 1',
        'bot 1 gives low to output 1 and high to bot 0',
        'bot 0 gives low to output 2 and high to output 0',
        'value 2 goes to bot 2',
    )

    look = (2, 5)
    the_bots, the_output, the_watches = part1(data=instructions, look=look)

    seq = (5, 2, 3)
    for num, val in enumerate(seq):
        assert val in the_output[num].items

    numbers = [bot.number for bot in the_watches]
    assert 2 in numbers
    import ipdb; ipdb.set_trace()  #XXX BREAKPOINT


def part1(data, look):
    """ assign items first ... """
    for number, line in enumerate(data):
        msg = 'Doing assignments: %s %s' % (number, line.strip())
        logger.debug(msg)

        """ readline """
        if 'value' in line:
            parse(line.strip(), look)

    for number, line in enumerate(data):
        msg = '%s %s' % (number, line.strip())
        logger.debug(msg)

        """ readline """
        if 'value' not in line:
            parse(line.strip(), look)

        """ turn on bots """
        for num, bot in sorted(Bots.items()):
            bot.started = True

    """ bots give stuff away this triggers a cascade
    since received, triggers giving away
    """
    bar = advance()

    return Bots, Outputs, Watches

if __name__ == '__main__':
    """ part1 """
    fqn = 'ten_data.txt'
    data = open(fqn, 'r').readlines()
    look = 61, 17

    the_bots, the_output, the_watches = part1(data=data,
            look=look)

    """
    2016-12-15 12:06:06,469:INFO:__main__:Watch found
    <Bot:86, items:[61, 17], low:('bot', 145) high:('bot', 71)
    look:(61, 17) >
    """

    """ part2 """
    stuff = [Outputs[bucket].items[0] for bucket in (0, 1, 2)]
    out = functools.reduce(operator.mul, stuff, 1)
    logger.info('Multiply bins 0,1,2 : %s' % (out))
