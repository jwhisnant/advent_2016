#!/usr/bin/env python
# -*- coding: utf-8 -*-

"""
File: test_ten.py
Author: yourname
Email: yourname@email.com
Github: https://github.com/yourname
Description:
"""

from ten import part1


def test_part1():

    instructions = (
        'value 5 goes to bot 2',
        'bot 2 gives low to bot 1 and high to bot 0',
        'value 3 goes to bot 1',
        'bot 1 gives low to output 1 and high to bot 0',
        'bot 0 gives low to output 2 and high to output 0',
        'value 2 goes to bot 2',
    )

    look = (2, 5)
    the_bots, the_output, the_watches = part1(data=instructions, look=look)

    seq = (5, 2, 3)
    for num, val in enumerate(seq):
        assert val in the_output[num].items

    numbers = [bot.number for bot in the_watches]
    assert 2 in numbers
