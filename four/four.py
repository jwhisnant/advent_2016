#!/usr/bin/env python
# -*- coding: utf-8 -*-

"""
File: four.py
Author: yourname
Email: yourname@email.com
Github: https://github.com/yourname
Description: 

--- Day 4: Security Through Obscurity ---

Finally, you come across an information kiosk with a list of rooms. Of course, the list is encrypted and full of decoy data, 
but the instructions to decode the list are barely hidden nearby. Better remove the decoy data first.

Each room consists of an encrypted name (lowercase letters separated by dashes) followed by a dash, a sector ID, and a checksum in square brackets.

A room is real (not a decoy) if the checksum is the five most common letters in the encrypted name, in order, with ties broken by alphabetization. 
For example:

    aaaaa-bbb-z-y-x-123[abxyz] is a real room because the most common letters are a (5), b (3), and then a tie between x, y, and z, which are listed alphabetically.
    a-b-c-d-e-f-g-h-987[abcde] is a real room because although the letters are all tied (1 of each), the first five are listed alphabetically.
    not-a-real-room-404[oarel] is a real room.
    totally-real-room-200[decoy] is not.

Of the real rooms from the list above, the sum of their sector IDs is 1514.

What is the sum of the sector IDs of the real rooms?
"""

import string
from collections import Counter

# logging
import logging
logging.basicConfig(format='%(asctime)s:%(levelname)s:%(name)s:%(message)s')
logger = logging.getLogger(__name__)
logger.setLevel(logging.INFO)


def name(chars):
    return ''.join(chars.split('-')[:-1])


def sector(chars):
    return int(chars.split('-')[-1].split('[')[0])


def checksum(chars):
    return chars.split('[')[-1][:-1]


def invert(counter):
    """ sigh """
    out = {}
    for value, number in counter.most_common():
        if number not in out:
            out[number] = []
        out[number].append(value)

    for key in out:
        out[key] = ''.join(sorted(out[key]))
    return out


def is_real(chars):
    _name = name(chars)
    _sector = sector(chars)
    _checksum = checksum(chars)

    counter = Counter(_name)
    inverted = invert(counter)

    temp = ''.join(value for key, value in sorted(
        inverted.items(), reverse=True))

    should_be = ''.join(temp)[:5]

    logger.debug('%s %s %s %s %s' %
                 (chars, _name, _sector, _checksum, should_be))

    if _checksum == should_be:
        return True

    return False


def sector_sum(rooms):
    total = 0
    for room in rooms:
        if is_real(room):
            total = total + sector(room)
    return total


def shift(chars, cycles):
    out = ''
    template = string.ascii_lowercase + string.ascii_lowercase
    for char in chars:

        if char in string.digits:
            return out.strip()

        if char != '-':
            move = cycles % 26
            loc = template.find(char)
            real = template[loc + move]
            out = out + real

        if char == '-':
            out = out + ' '

    return out.strip()


def shifty(rooms):
    """ I guess we have to read for answer manually """
    out = []
    for room in rooms:
        if is_real(room):
            _sector = sector(room)
            word = shift(room, _sector)
            out.append((word, _sector, room))
    return out

if __name__ == '__main__':
    rooms = [x.strip() for x in open('four_data.txt', 'r')]
    logger.info('Sector sum: %s' % (sector_sum(rooms)))

    # part II
    words = shifty(rooms)

    for num, (word, sector, x) in enumerate(sorted(words)):
        val = 'Word: (%s), sector: (%s)' % (word, sector)
        logger.debug(val)

        if 'northpole object storage' in word:
            logger.info(val)
