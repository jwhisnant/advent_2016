
# -*- coding: utf-8 -*-

"""
File: test_four.py
Author: yourname
Email: yourname@email.com
Github: https://github.com/yourname
Description: 

--- Day 4: Security Through Obscurity ---

Finally, you come across an information kiosk with a list of rooms. Of course, the list is encrypted and full of decoy data, 
but the instructions to decode the list are barely hidden nearby. Better remove the decoy data first.

Each room consists of an encrypted name (lowercase letters separated by dashes) followed by a dash, a sector ID, and a checksum in square brackets.

A room is real (not a decoy) if the checksum is the five most common letters in the encrypted name, in order, with ties broken by alphabetization. 
For example:

    aaaaa-bbb-z-y-x-123[abxyz] is a real room because the most common letters are a (5), b (3), and then a tie between x, y, and z, which are listed alphabetically.
    a-b-c-d-e-f-g-h-987[abcde] is a real room because although the letters are all tied (1 of each), the first five are listed alphabetically.
    not-a-real-room-404[oarel] is a real room.
    totally-real-room-200[decoy] is not.

Of the real rooms from the list above, the sum of their sector IDs is 1514.

What is the sum of the sector IDs of the real rooms?
"""

first = 'aaaaa-bbb-z-y-x-123[abxyz]'
second = 'a-b-c-d-e-f-g-h-987[abcde]'
third = 'not-a-real-room-404[oarel]'
fourth = 'totally-real-room-200[decoy]'

from four import (
    checksum,
    is_real,
    name,
    sector,
    sector_sum,
    shift,
)


def test_names():
    assert name(first) == 'aaaaabbbzyx'
    assert name(second) == 'abcdefgh'
    assert name(third) == 'notarealroom'
    assert name(fourth) == 'totallyrealroom'


def test_sectors():
    assert sector(first) == 123
    assert sector(second) == 987
    assert sector(third) == 404
    assert sector(fourth) == 200


def test_checksums():
    assert checksum(first) == 'abxyz'
    assert checksum(second) == 'abcde'
    assert checksum(third) == 'oarel'
    assert checksum(fourth) == 'decoy'


def test_isreal():
    assert is_real(first) == True
    assert is_real(second) == True
    assert is_real(third) == True
    assert is_real(fourth) == False


def test_sector_sum():
    rooms = (first, second, third, fourth)
    assert sector_sum(rooms) == 1514


def test_shift():
    val = 'qzmt-zixmtkozy-ivhz-343[IDONTCARE]'
    out = 'very encrypted name'
    assert shift(val, 343) == out
