#!/usr/bin/env python
# -*- coding: utf-8 -*-

"""
File: five.py
Author: yourname
Email: yourname@email.com
Github: https://github.com/yourname
Description:
"""

import itertools
import hashlib

# logging
import logging
logging.basicConfig(format='%(asctime)s:%(levelname)s:%(name)s:%(message)s')
logger = logging.getLogger(__name__)
logger.setLevel(logging.DEBUG)
#logging.getLogger('sqlalchemy.engine.base').setLevel(logging.DEBUG)

from tqdm import tqdm


class Hasher(object):

    def __init__(self,
            text,
            start=0,
            stop=None,
            algo='md5',
            password=None,
            fqn='five.out',
            current_position=None,
                 ):
        """TODO: Docstring for __init__.

        Parameters
        ----------
        text : TODO
        start : TODO, optional
        """
        self.text = text
        self.start = start
        self.stop = stop
        self.algo = algo
        self.password = password

        if self.password is None:
            self.password = []

        self.fqn = fqn

        self.current_position = current_position
        if self.current_position is None:
            self.current_position = self.start

        self.total = 3 * 1000 * 1000 * len(self.text)

    def __call__(self):
        """doc"""

        self.data = open(self.fqn, 'w')

        for number in tqdm(itertools.count(self.start), total=self.total):
            candidate = self.text + str(self.current_position)

            out = self.hash(candidate)
            self.update_pw(out)

            self.current_position = self.current_position + 1

            if len(self.password) == 8 and None not in self.password:
                return self

            if self.stop is not None:
                if number >= self.stop:
                    return self

        """ will never get here """
        return self

    def update_pw(self, out):
        if out[:5] == '00000':
            char = self.hash2char(out)
            tqdm.write(char)
            self.data.write(char)
            self.data.flush()
            self.password.append(char)

    def hash(self, candidate):
        bar = hashlib.new(self.algo)
        bar.update(candidate)
        out = bar.hexdigest()
        return out

    def hash2char(self, hexdigest):
        return hexdigest[5]


class SeqHasher(Hasher):

    """
    now the six char returns position
    """

    def __init__(self,
            text,
            start=0,
            stop=None,
            algo='md5',
            password=None,
            fqn='five.out',
            current_position=None,
                 ):

        super(SeqHasher, self).__init__(
            text=text,
            start=start,
            stop=stop,
            algo=algo,
            password=password,
            fqn=fqn,
            current_position=current_position,
        )

        self.total = 5 * 1000 * 1000 * len(self.text)

    def update_pw(self, out, pos=6):
        if out[:5] == '00000':
            tqdm.write('found %s at %s' % (out, self.current_position))

            loc = self.hash2char(out)

            try:
                loc = int(loc)
                char = out[pos]

            except ValueError:
                pass
                return None

            if loc not in range(0, 8):
                tqdm.write('not %s (out of range)' % (loc))
                return None

            if loc in range(0, 8):
                if self.password[loc] is not None:
                    tqdm.write('not %s at %s (already have %s)' % (char, loc,
                        self.password[loc]))

                if self.password[loc] is None:

                    self.password[loc] = char

                    tqdm.write(repr(self.password))
                    self.data.write(repr(self.password))
                    self.data.flush()


if __name__ == '__main__':
    """ part 1 """
    # hasher=Hasher(text='abc',
            # start=3231900,
            # )

    # hasher=Hasher(text='ffykfhsq')
    # worked=hasher()
    # logger.info(''.join(worked.password))

    """part 2"""
    # hasher=SeqHasher(text='abc',
            # start=0,
            # password=[None]*8,
            # )

    # worked=hasher()

    text = 'ffykfhsq'
    hasher = SeqHasher(
        text=text,
        fqn='five_two.out',
            password=[None] * 8,
            )

    worked = hasher()

    logger.info(''.join(worked.password))
