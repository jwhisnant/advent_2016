#!/usr/bin/env python
# -*- coding: utf-8 -*-

"""
File: test_five.py
Author: yourname
Email: yourname@email.com
Github: https://github.com/yourname
Description:
"""

from five import Hasher, SeqHasher


def test_hash():

    hasher = Hasher('abc')

    out = hasher.hash('abc3231929')
    assert out[:5] == '00000'
    assert(hasher.hash2char(out)) == '1'

    out = hasher.hash('abc5017308')
    assert out[:5] == '00000'
    assert(hasher.hash2char(out)) == '8'

    out = hasher.hash('abc5278568')
    assert out[:5] == '00000'
    assert(hasher.hash2char(out)) == 'f'


def test_part2():
    """ one,ok """
    hasher = SeqHasher(text='abc',
                       start=3231929,
                      stop=3231929 + 1,
            password=[None] * 8,
    )

    worked = hasher()
    assert worked.password[1] == '5'

    """ two, invalid position """
    hasher = SeqHasher(text='abc',
            start=5017308,
            stop=5017308 + 1,
            password=[None] * 8,
    )
    worked = hasher()
    assert not any(worked.password)

    """ three """
    hasher = SeqHasher(text='abc',
            start=5357525,
            stop=5357525 + 1,
            password=[None] * 8,
    )

    worked = hasher()
    assert worked.password[4] == 'e'
