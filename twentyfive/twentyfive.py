#!/usr/bin/env python
# -*- coding: utf-8 -*-

"""
File: twentyfive.py
Author: yourname
Email: yourname@email.com
Github: https://github.com/yourname
Description:
"""

# logging
import logging
logging.basicConfig(format='%(asctime)s:%(levelname)s:%(name)s:%(message)s')
logger = logging.getLogger(__name__)
logger.setLevel(logging.DEBUG)

from pprint import pprint as pp
from tqdm import tqdm


def get_int(val, registers):
    if val not in registers:
        return int(val)

    if val in registers:
        return registers[val]


def out(ins, registers, instructions=None, pos=None):
    """
    return the next func location
    """
    temp = ins.split()
    x = temp[1]

    val = get_int(x, registers)

    return 1, registers, instructions, val


def cpy(ins, registers, instructions=None, pos=None):
    """
    return the next func location
    """
    temp = ins.split()
    x = temp[1]
    y = temp[2]

    if x in registers:
        x = registers[x]

    registers[y] = int(x)
    return 1, registers, instructions, None


def inc(ins, registers, instructions=None, pos=None):
    """
    return the next func location
    """

    temp = ins.split()
    x = temp[1]
    registers[x] = registers[x] + 1

    return 1, registers, instructions, None


def dec(ins, registers, instructions=None, pos=None):
    """
    return the next func location
    """

    temp = ins.split()
    x = temp[1]
    registers[x] = registers[x] - 1

    return 1, registers, instructions, None


def jnz(ins, registers, instructions=None, pos=None):
    """
    return the next func location
    """
    temp = ins.split()
    x = temp[1]
    y = temp[2]

    x = get_int(x, registers)
    y = get_int(y, registers)

    if x not in registers:
        if int(x):
            return int(y), registers, instructions, None

    if x in registers:
        val = registers[x]
        if val:
            return int(y), registers, instructions, None

    return 1, registers, instructions, None


def two(target):

    if 'inc' in target:
        target = target.replace('inc', 'dec')
        return target

    if 'inc' not in target:
        temp = target.split()
        temp[0] = 'inc'
        target = ' '.join(temp)

    return target


def three(target):

    if 'jnz' in target:
        target = target.replace('jnz', 'cpy')
        return target

    if 'jnz' not in target:
        temp = target.split()
        temp[0] = 'jnz'
        target = ' '.join(temp)

    return target


def rewrite(instructions, where):
    """ tgl rewrites instructions """
    try:
        target = instructions[where]
    except IndexError: # do nothing if out of range
        return instructions

    if len(target.split()) == 2:
        target = two(target)

    if len(target.split()) == 3:
        target = three(target)

    """ update"""
    instructions[where] = target

    return instructions


def tgl(ins, registers, instructions=None, pos=None):
    """
    return the next func location
    """
    temp = ins.split()
    loc = temp[1]

    if loc not in registers:
        val = int(loc)

    if loc in registers:
        val = registers[loc]

    where = int(val) + pos
    instructions = rewrite(instructions, where)

    return 1, registers, instructions, None


def signal_ok(signals):
    odds = signals[0::2]
    evens = signals[1::2]

    if sum(odds) == 0:
        if evens:
            if sum(evens) / len(evens) == 1:
                return True
        if not evens:
            return True

    return False


def runner(instructions,
           registers,
           dispatch,
           save_state=False,
           howmany=1000):

    _instructions = tuple(instructions)

    pos = 0
    count = 0
    states = []

    signals = []
    a = registers.get('a')

    if 1:
    # with tqdm() as pbar:
        while pos <= len(instructions):
            # logger.info('pos now: %s'%(pos) )
            try:
                ins = instructions[pos]
            except IndexError as err:
                # XXX TODO: BunnyHopException 2016/12/15  (james)
                logger.info('end of program')
                # logger.exception(err)

                val = "(%s) (%s) pos:%s next:%s, ins:'%s' reg:%s" % (
                    len(instructions), count, pos, pos + res, ins, registers.copy())
                logger.info(val)

                return registers, states

            name = ins.split()[0]
            func = dispatch[name]
            res, registers, instructions, signal = func(ins, registers, instructions, pos)

            if signal is not None: # yes, not None ...
                signals.append(signal)
                ok = signal_ok(signals)
                if not ok:
                    signal = ''.join(str(x) for x in signals)
                    msg = 'FAIL number: %s registers: %s signals:%s' % (a, registers, signal)
                    # logger.info(msg)

                    return None, states

            pos = pos + res

            val = "(%s) (%s) pos:%s next:%s, ins:'%s' reg:%s" % (
                len(instructions), count, pos, pos + res, ins, registers.copy())
            #logger.info(val)
            # logger.info(instructions)

            if save_state:
                states.append((registers, val))

            if pos < 0: #under bounds
                pos = 0

            if pos == len(instructions):
                pass
                """ idc """

            if pos > len(instructions):
                """ idc """
                logger.info(val)
                return registers, states

            count = count + 1
            # pbar.update(count)

            # close enough to infinite ...
            if len(signals) > howmany:
                return signals, states

    return registers, states


def run_part1(save_state=False):
    instructions = open('twentyfive_data.txt', 'r').readlines()

    _instructions = tuple(instructions)
    dispatch = {'cpy': cpy,
                'inc': inc,
                'dec': dec,
                'jnz': jnz,
                'tgl': tgl,
                'out': out,
                }

    cleaned = [x.strip() for x in instructions if x.strip()]

    for x in tqdm(range(0, 10 * 10 * 5)):
        registers = {'a': x,
                     'b': 0,
                     'c': 0,
                     'd': 0,
                     }

        signals, states = runner(cleaned, registers, dispatch, save_state)

        if signals:
            signal = ''.join(str(x) for x in signals)
            msg = 'SUCCESS number: %s signals: %s registers:%s' % (x, signal, registers)
            logger.info(msg)

            return signals, states


def run_part2(registers, save_state=False):
    """ as in part 1, only a=12 to start """
    instructions = open('twentyfive_data.txt', 'r').readlines()

    _instructions = tuple(instructions)
    dispatch = {'cpy': cpy,
                'inc': inc,
                'dec': dec,
                'jnz': jnz,
                'tgl': tgl,
                }
    cleaned = [x.strip() for x in instructions if x.strip()]

    return runner(cleaned, registers, dispatch, save_state)

if __name__ == '__main__':
    signal_ok((0, 1, 0, 1))
    """ part 1 """

    registers, states = run_part1(save_state=False)

    """ part 2 """

    # registers = {'a': 12,
                 # 'b': 0,
                 # 'c': 0,
                 # 'd': 0,
                 # }

    # registers, states = run_part2(registers=registers, save_state=False)
