#!/usr/bin/env python
# -*- coding: utf-8 -*-

"""
File: test_nineteen.py
Author: yourname
Email: yourname@email.com
Github: https://github.com/yourname
Description:
"""

# logging
import logging
logging.basicConfig(format='%(asctime)s:%(levelname)s:%(name)s:%(message)s')
logger = logging.getLogger(__name__)
logger.setLevel(logging.DEBUG)


from collections import deque

from nineteen import (
    agg,
    circle_steal,
    mk_cue,
    mk_dict,
    part1,
    part2,
)

from nineteen import GiftExchange

""" part 1 """


def test_mk_cue():
    """ False or location of winner """
    assert mk_cue(5) == [[1, 1], [2, 1], [3, 1], [4, 1], [5, 1]]


def test_agg_5():
    cue = mk_cue(5)

    out = agg(cue)
    assert out == [[3, 2], [5, 3]]

    out = agg(out)
    assert out == [[3, 5]]


def test_agg_7():
    """ prior algo did this wrong """
    cue = mk_cue(7)

    out = agg(cue)
    assert out == [[3, 2], [5, 2], [7, 3]]

    out = agg(out)
    assert out == [[7, 7]]


def test_part1():
    assert part1(5) == [[3, 5]]

""" part2 """


def test_mk_dict():
    """ False or location of winner """
    assert mk_dict(5) == {1: 1, 2: 1, 3: 1, 4: 1, 5: 1}


def test_circlesteal():
    items = mk_cue(5)

    """ adict for state storage """
    adict = {}
    for num, val in enumerate(items, start=1):
        adict[num] = val

    items, winner, loser_index = circle_steal(items, adict.get(1))
    assert items == [[1, 2], [2, 1], [4, 1], [5, 1]]
    assert winner == [1, 2]
    assert loser_index == 3

    del adict[loser_index]

    items, winner, loser_index = circle_steal(items, adict.get(2))
    assert items == [[1, 2], [2, 2], [4, 1]]
    assert winner == [2, 2]
    assert loser_index == 5
    del adict[loser_index]

    """ 3 gets skipped, he swims with the fishes"""
    items, winner, loser_index = circle_steal(items, adict.get(4))
    assert items == [[2, 2], [4, 3]]
    assert winner == [4, 3]
    assert loser_index == 1
    del adict[loser_index]

    """ 5 gets skipped, as does 1, they left the building with Elvis """
    items, winner, loser_index = circle_steal(items, adict.get(2))
    assert items == [[2, 5]]
    assert winner == [2, 5]
    assert loser_index == 4
    del adict[loser_index]


### re-implement part 2

def test_gift_functions():
    number = 5
    regifter = GiftExchange(number=number, record=True)

    regifter.split()
    assert regifter.winners == deque([[1, 1], [2, 1]])

    """ we want floor division here ... """
    assert len(regifter.winners) == len(regifter.elves) / 2

    """ all elves are potential "losers", and we shift """
    assert regifter.losers == deque([[3, 1], [4, 1], [5, 1], [1, 1], [2, 1]])
    assert len(regifter.losers) == len(regifter.elves)

    regifter.one_round()
    """ 1 takes from 3, and 2 from 5 """
    assert regifter.transactions == [((1, 2), (3, 0)), ((2, 2), (5, 0))]
    assert regifter.num_removed == len(regifter.transactions)

    regifter.one_round()
    """ 4 takes from 1 """
    assert regifter.transactions == [((1, 2), (3, 0)), ((2, 2), (5, 0)), ((4, 3), (1, 0))]

    regifter.one_round()
    """ 2 takes from 4 and wins """
    assert regifter.transactions == [((1, 2), (3, 0)), ((2, 2), (5, 0)), ((4, 3), (1, 0)), ((2, 5), (4, 0))]


def test_giftexchange():
    """ test gift exchanges, 5 is our given test case
    we test some others as well we can do by hand
    """

    # degenerate case
    number = 1
    regifter = GiftExchange(number=number)
    gift = regifter()
    assert gift.elves == deque([[1, 1]])
    assert gift.elves[0][1] == number

    number = 2
    regifter = GiftExchange(number=number)
    gift = regifter()
    assert gift.elves == deque([[1, 2]])
    assert gift.elves[0][1] == number

    number = 3
    regifter = GiftExchange(number=number)
    gift = regifter()
    assert gift.elves == deque([[3, 3]])
    assert gift.elves[0][1] == number

    number = 4
    regifter = GiftExchange(number=number)
    gift = regifter()
    assert gift.elves == deque([[1, 4]])
    assert gift.elves[0][1] == number

    number = 5
    regifter = GiftExchange(number=number)
    gift = regifter()
    assert gift.elves == deque([[2, 5]])
    assert gift.elves[0][1] == number

    # number = 6
    # regifter = GiftExchange(number=number)
    # gift = regifter()
    # assert gift.elves == deque([[3, 6]])
    # assert gift.elves[0][1] == number

    # number = 7
    # regifter = GiftExchange(number=number)
    # gift = regifter()
    # assert gift.elves == deque([[6, 7]])
    # assert gift.elves[0][1] == number
