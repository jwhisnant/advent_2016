from nineteen import GiftExchange, part2
import csv

# logging
import logging
logging.basicConfig(format='%(asctime)s:%(levelname)s:%(name)s:%(message)s')
logger = logging.getLogger(__name__)
logger.setLevel(logging.DEBUG)


def second_test(start, stop):
    out = []
    for puzzle_input in range(start, stop):

        """ part 2 """
        regifter = GiftExchange(number=puzzle_input)
        gifted = regifter()

        winner, presents = gifted.elves[0]
        msg = 'Elf %s won with %s presents' % (winner, presents)
        logger.info(msg)
        out.append('%s, %s' % (puzzle_input, winner))
    return out


def two(start, stope):
    out = second_test(start, stop)
    foo = '\n'.join(out)
    open('/tmp/two-results.csv', 'w').writelines(foo)


def one(start, stop):
    out = first_test(start, stop)
    foo = '\n'.join(out)
    open('/tmp/one-results.csv', 'w').writelines(foo)


def first_test(start, stop):
    out = []
    for puzzle_input in range(start, stop):
        adict = part2(puzzle_input)
        out.append('%s, %s' % (puzzle_input, adict.keys()[0]))
    return out


if __name__ == '__main__':
    start = 2
    stop = 200

    two(start, stop)
    one(start, stop)
