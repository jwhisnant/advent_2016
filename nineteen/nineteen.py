#!/usr/bin/env python
# -*- coding: utf-8 -*-

"""
File: nineteen.py
Author: yourname
Email: yourname@email.com
Github: https://github.com/yourname
Description:
"""

import itertools
import json
import os


from collections import deque
from profilehooks import profile
from tqdm import tqdm


# logging
import logging
logging.basicConfig(format='%(asctime)s:%(levelname)s:%(name)s:%(message)s')
logger = logging.getLogger(__name__)
logger.setLevel(logging.INFO)


def mk_cue(number):
    out = []
    for num in range(1, number + 1):
        out.append([num, 1])
    return out


def mk_dict(number):
    out = {}
    for num in range(1, number + 1):
        out[num] = 1
    return out


def agg(cue):
    out = []
    winners = cue[0::2]
    losers = cue[1::2]

    """ assuming list creation is shorter than a conditional
    check for every iteration
    """
    to_add = list(itertools.izip_longest(winners, losers))

    """
    the last person needs to steal from the first
    sometimes, so handle that at the end ....
    """

    for win, lose in to_add[:-1]:
        win[1] = win[1] + lose[1]
        out.append(win)

        """ nobody likes a loser """
        #lose[1]=0
        #out.append(lose)

    last, other = to_add[-1]

    if other is None:
        """ and the last shall be first """
        first = out[0]

        last[1] = last[1] + first[1]
        first[1] = 0

        out.append(last)
        out.pop(0)

    if other is not None:
        last[1] = other[1] + last[1]
        out.append(last)

    # logger.debug(repr(out))
    return out


def find_left(items, where):
    """ does not work ... """
    for num, candidate in enumerate(items[where::-1]):

        """ i can haz presents! """
        if candidate[1]:
            return candidate, where - num

    return None, None


#@profile
def circle_steal(items, me, index=None): #, removed=0):
    """ items is a list of elves
    me is a tuple, whose turn it is
    """

    if index is None:
        # XXX TODO: implement better 2016/12/21  (jwhisnant)
        whereami = items.index(me)

    if index is not None:
        """ clearly we know our index at the start ... """
        # XXX TODO: implement better 2016/12/21  (jwhisnant)
        whereami = index

    ahead = len(items) / 2
    where = whereami + ahead

    """ handle wrapping """
    where = where % len(items)

    #target, loser_index = find_left(items, where)

    target = items[where]

    """ steal """
    items[whereami][1] = items[whereami][1] + target[1]
    items[where][1] = 0
    #items[loser_index][1]=0

    winner = items[whereami][:]

    msg = 'elf %s stole from elf:%s num:%s' % (me[0], target[0], target[1])
    logger.info(msg)

    """ Whats that! Get out! """
    # this is ... slow ... for large lists
    items.pop(where)

    """ return removed, since we need to update dict """
    loser_index = target[0]

    return items, winner, loser_index


def part1(puzzle_input):

    cue = mk_cue(puzzle_input)

    with tqdm() as pbar:
        while len(cue) > 1:
            cue = agg(cue)
            pbar.update()

    # logger.info(repr(cue))
    msg = 'Part 1 : Elf %s won with %s presents' % (cue[0][0], cue[0][1])
    # logger.debug(msg)

    return cue


def _clean(adict, remove):
    del adict[remove]
    return adict


def _part2(items, adict, numbers):

    removed = 0
    for index, num in enumerate(tqdm(numbers)):
        me = adict.get(num) or None

        if me:
        # if items[index][1]:
            # me=items[index]
            items, won, removed_index = circle_steal(items, me,
                                                     index=None,
                                                     # removed=removed
                                                     )

            adict = _clean(adict, removed_index)
            # pbar.update()

            removed = removed + 1

            if len(adict) == 1:
                return items, adict

    fn = '%s-%s-temp.json' % (removed, len(items))
    fqn = os.path.join('/tmp/', fn)
    json.dump(items, open(fqn, 'w'))
    tqdm.write('Wrote %s lines to %s' % (len(items), fqn))

    # this has the info about who was removed ...
    return items, adict


def part2(puzzle_input):
    """ a list to do work on  """
    items = mk_cue(puzzle_input)

    """ adict for state storage """
    adict = {}
    for num, val in enumerate(items, start=1):
        adict[num] = val

    numbers = xrange(1, puzzle_input + 1)

    while len(adict) > 1:
        items, adict = _part2(items, adict, numbers)

        """ reduce numbers to search, we know who left in the last round """
        numbers = sorted(adict.keys())

    msg = 'Part 2: Elf winner: %s' % (repr(adict))
    logger.info(msg)
    return adict


class GiftExchange(object):

    def __init__(self,
                 number,
                 transactions=None,
                 record=False,
                 num_removed=0,
                 ):

        self.number = number
        self.transactions = transactions
        self.record = record
        self.num_removed = num_removed

        self.removed = []

        self.last_elf = 1

        if not self.transactions:
            self.transactions = []

        self.setup()

    #@profile(entries=100)
    def __call__(self):

        while len(self.elves) > 1:
            self.one_round()

        return self

    def find_next(self):
        """ shift until next number first in list """
        self.removed.sort()
        search = sorted([number for number, amt in self.elves])
        for number in search:
            if number > self.last_elf:
                return number
        return None

    def one_round(self):
        """ find next avail number """
        until = self.find_next()
        if until:
            while self.elves[0][0] > until:
                self.elves.rotate(1)

        """ make 2 groups """
        self.split()

        """ change self.winners and self.losers """
        self.exchange()

        """ losers have all the elf states, use them ... """
        self.elves = self.prune(self.losers)

        """ clear winners and losers """
        self.winners = None
        self.losers = None

    def setup(self):
        self.losers = deque()
        cue = mk_cue(self.number)
        self.elves = deque(cue)

    def swap(self, taker, giver):
        """ doc """

        taker[1] = taker[1] + giver[1]
        giver[1] = 0

        return taker, giver

    def prune(self, elves):
        """ kick out the losers """
        survivors = [elf for elf in elves if elf[1]]
        return deque(survivors)

    def split(self):
        half = len(self.elves) / 2
        self.winners = deque(list(self.elves)[:half])

        self.losers = deque(self.elves)
        self.losers.rotate(-half)

    def shift(self, removed):
        """ shift losers to the left by one every time!"""
        # num = len(self.elves) - self.num_removed
        num = len(self.elves) - removed

        if num % 2 == 0: # even
            self.losers.rotate(-1)

    def exchange(self):
        removed = 0
        for num, elf in enumerate(tqdm(self.winners)):

            """ exchange gifts """
            winner, loser = self.swap(elf, self.losers[num])
            self.winners[num] = winner
            self.losers[num] = loser

            msg = 'elf %s stole from elf:%s num:%s' % (winner[0],
                                                       loser[0],
                                                       loser[1])
            logger.info(msg)

            if self.record:
                self.transactions.append((tuple(winner), tuple(loser)))
                # logger.info(self.transactions)

            # self.num_removed = self.num_removed + 1
            removed = removed + 1
            self.removed.append(loser[0])

            """ rotate losers """
            self.shift(removed)

        msg = '%s elves will be removed' % (len(self.winners))
        logger.info(msg)

        self.last_elf = winner[0]

if __name__ == '__main__':

    test_puzzle_input = 4
    puzzle_input = 3014603

    """ part 1 """
    # part1(puzzle_input)

    """ part 2 """
    regifter = GiftExchange(number=test_puzzle_input, record=True)
    gifted = regifter()

    winner, presents = gifted.elves[0]
    msg = 'Elf %s won with %s presents' % (winner, presents)
    logger.info(msg)

    # too high ...
    #2016-12-22 10:08:25,425:INFO:__main__:Elf 2224166 won with 3014603 presents

    """ poor (but accurate, we think) part 2 implemenation, it was ..."""
    # part2(test_puzzle_input)
