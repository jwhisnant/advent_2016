#!/usr/bin/env python
# -*- coding: utf-8 -*-

"""
File: seven.py
Author: yourname
Email: yourname@email.com
Github: https://github.com/yourname
Description:
"""

from collections import Counter

# logging
import logging
logging.basicConfig(format='%(asctime)s:%(levelname)s:%(name)s:%(message)s')
logger = logging.getLogger(__name__)
logger.setLevel(logging.INFO)


def pallindrome(block,size=4):
    """
    there can be more than 
    one pallindrome in
    a block!
    """
    vals = []

    for number, chars in enumerate(block):
        val = block[number:number + size]
        if val == ''.join(reversed(val)):
            if len(Counter(val)) == 2:
                if val and len(val) == size:
                    vals.append(val)

    return vals


def string2blocks(astring):
    blocks = []
    temp = []

    for char in astring:
        if char not in ('[', ']'):
            temp.append(char)

        if char in ('[', ):
            blocks.append((''.join(temp), 'outside'))
            temp = []

        if char in (']', ):
            blocks.append((''.join(temp), 'inside'))
            temp = []

    blocks.append((''.join(temp), 'outside'))
    return blocks


def build_dict(data):
    test = {}
    for (drome, where) in data:

        if where not in test:
            test[where] = []

        test[where].append(drome)

    return test


def is_ssl(adict):
    """
    this one is tricky
    we must have an inner, and a "target" in outer
    """
    outside = adict.get('outside', ()) or ()
    inside = adict.get('inside', ()) or ()

    outside=[x for x in outside if x]
    inside=[x for x in inside if x]

    """ we must have an inner """

    if not any(inside):
        return False

    for item in inside:
        target=item[1]+item[0]+item[1]
        if target in outside:
            return True

    return False

def is_tls(adict):
    outside = adict.get('outside', ()) or ()
    inside = adict.get('inside', ()) or ()

    if any(inside):
        return False

    if any(outside) and not(any(inside)):
        return True

    if not any(outside) and not(any(inside)):
        return False

    """ should not happen """
    return None

def tls(text):
    return main(text, size=4, fun=is_tls)

def ssl(text):
    return main(text, size=3,fun=is_ssl)

def main(text,size, fun):
    foo = string2blocks(text)

    data = []
    for block, where in foo:

        dromes = pallindrome(block, size)
        for drome in dromes:
            data.append((drome, where))

    adict = build_dict(data)
    result = fun(adict)

    val = '%s %s %s' % (result, data, text)
    logger.debug(val)

    # if result:
        # logger.info(val)

    return result


def _test_part1():
    data = (
        ('abba[mnop]qrst', True),
        ('abcd[bddb]xyyx', False),
        ('aaaa[qwer]tyui', False),
        ('ioxxoj[asdfgh]zxcvbn', True),
    )

    for text, val in data:
        assert tls(text) == val

def part1():
    """ part 1 """
    out = []
    for line in open('seven_data.txt', 'r').readlines():
        out.append(tls(line.strip()))

    counter = Counter(out)
    # 214 is too high ... bug with val!=4
    # 82 is too low ... bug with not having correct data
    # 110
    val = '%s items support TLS' % (counter.get(True))
    logger.info(val)


def _test_part2():

    data = (
        # ('aba[bab]xyz', True),
        # ('xyx[xyx]xyx', False),
        # ('aaa[kek]eke', True),
        ('zazbz[bzb]cdb', True),
    )

    for text, val in data:
        assert ssl(text) == val

def part2():
    """ part 2 """
    # 2 is too low - we did not allow for multiples in a single block
    out = []
    for line in open('seven_data.txt', 'r').readlines():
        out.append(ssl(line.strip()))

    counter = Counter(out)
    val = '%s items support SSL' % (counter.get(True))
    logger.info(val)


if __name__ == '__main__':
    part1()
    part2()

    
