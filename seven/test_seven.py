#!/usr/bin/env python
# -*- coding: utf-8 -*-

"""
File: test_seven.py
Author: yourname
Email: yourname@email.com
Github: https://github.com/yourname
Description:


    abba[mnop]qrst supports TLS (abba outside square brackets).
    abcd[bddb]xyyx does not support TLS (bddb is within square brackets, even though xyyx is outside square brackets).
    aaaa[qwer]tyui does not support TLS (aaaa is invalid; the interior characters must be different).
    ioxxoj[asdfgh]zxcvbn supports TLS (oxxo is outside square brackets, even though it's within a larger string).
"""
from seven import (
    pallindrome,
    main,
    string2blocks,
    build_dict,
    tls,
    ssl,
)


def test_pallindrome():
    assert pallindrome('xoox') == ['xoox', ]
    assert pallindrome('aaxooxjj') == ['xoox', ]
    assert pallindrome('aaaaaaaa') == []
    assert pallindrome('a') == []

    """ must be at least 4 chars in the default case """
    assert pallindrome('cbc') == []

    """ can be "size" characters long """
    assert pallindrome('cbc', size=3) == ['cbc', ]

    """ multiples can occur in a single block"""
    assert pallindrome('zazbz', size=3) == ['zaz', 'zbz']


def test_string2blocks():
    data = (
        ('abba[mnop]qrst', [('abba', 'outside'), ('mnop', 'inside'), ('qrst', 'outside')]),
        ('abcd[bddb]xyyx', [('abcd', 'outside'), ('bddb', 'inside'), ('xyyx', 'outside')]),
        ('aaaa[qwer]tyui', [('aaaa', 'outside'), ('qwer', 'inside'), ('tyui', 'outside')]),
        ('ioxxoj[asdfgh]zxcvbn', [('ioxxoj', 'outside'), ('asdfgh', 'inside'), ('zxcvbn', 'outside')]),
    )

    for text, val in data:
        assert string2blocks(text) == val


def test_build_dict():
    data = [('abba', 'outside'), ('', 'inside'), ('', 'inside')]
    assert build_dict(data) == {'inside': ['', ''], 'outside': ['abba']}

    data = [('', 'outside'), ('bddb', 'inside'), ('', 'inside')]
    assert build_dict(data) == {'inside': ['bddb', ''], 'outside': ['']}

    data = [('', 'outside'), ('', 'inside'), ('', 'inside')]
    assert build_dict(data) == {'inside': ['', ''], 'outside': ['']}

    data = [('oxxo', 'outside'), ('', 'inside'), ('', 'inside')]
    assert build_dict(data) == {'inside': ['', ''], 'outside': ['oxxo']}


def test_tls():
    """
    abba[mnop]qrst supports TLS (abba outside square brackets).
    abcd[bddb]xyyx does not support TLS (bddb is within square brackets, even though xyyx is outside square brackets).
    aaaa[qwer]tyui does not support TLS (aaaa is invalid; the interior characters must be different).
    ioxxoj[asdfgh]zxcvbn supports TLS (oxxo is outside square brackets, even though it's within a larger string).
    """

    data = (
        ('abba[mnop]qrst', True),
        ('abcd[bddb]xyyx', False),
        ('aaaa[qwer]tyui', False),
        ('ioxxoj[asdfgh]zxcvbn', True),
    )

    for astring, result in data:
        assert(tls(astring)) == result


def test_ssl():
    """
    aba[bab]xyz supports SSL (aba outside square brackets with corresponding bab within square brackets).
    xyx[xyx]xyx does not support SSL (xyx, but no corresponding yxy).
    aaa[kek]eke supports SSL (eke in supernet with corresponding kek in hypernet; the aaa sequence is not related, because the interior character must be different).
    zazbz[bzb]cdb supports SSL (zaz has no corresponding aza, but zbz has a corresponding bzb, even though zaz and zbz overlap).
    """

    data = (
        ('aba[bab]xyz', True),
        ('xyx[xyx]xyx', False),
        ('aaa[kek]eke', True),
        ('zazbz[bzb]cdb', True),
    )

    for astring, result in data:
        assert(ssl(astring)) == result
