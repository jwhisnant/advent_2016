#!/usr/bin/env python
# -*- coding: utf-8 -*-

"""
File: one.py
Author: jwhisnant
Email: jwhisnant@email.com
Github: https://github.com/jwhisnant
Description:
    https://adventofcode.com/2016/day/1
    How many blocks away is Easter Bunny HQ?

    They changed the rules! Darn it!
"""
import os

# logging
import logging
logging.basicConfig(format='%(asctime)s:%(levelname)s:%(name)s:%(message)s')
logger = logging.getLogger(__name__)
logger.setLevel(logging.DEBUG)


class Vector:

    def __init__(self, fqn=None, data=None, start=None):
        """
        data is for testing if not an fqn
        """
        self.fqn = fqn
        self.data = data
        self.start=start

        if self.start is None:
            self.start=[0,0]


        self.degrees = 0 # North
        self.val = (None, None)

        self.coords = []
        self.coords.append(self.start)

        self.moves = {0: [],
                      90: [],
                      }

        self.blocks = None
        self.new_blocks = None

        self.expansion = []

    def __call__(self):

        if self.fqn and not self.data:
            self.data = self.read_file()

        self.build_dict()

        """ Part 1 """
        self.blocks = self.calculate()

        """ Part 2 """
        self._expansion()
        self.get_target()

        x, y = self.target

        if x and y:
            self.new_blocks = abs(x) + abs(y)

        """ just because """
        return self

    def _expansion(self):
        """ this is for Part 2 """
        for pair in zip(self.coords, self.coords[1:]):
            x1, y1 = pair[0]
            x2, y2 = pair[1]

            if x1 <x2 and y1 == y2:
                for val in xrange(x1, x2):
                    self.expansion.append([val, y1])

            if x1 >x2 and y1 == y2:
                for val in xrange(x1, x2,-1):
                    self.expansion.append([val, y1])

            if x1 == x2 and y1 < y2:
                for val in xrange(y1, y2):
                    self.expansion.append([x1, val])

            if x1 == x2 and y1 > y2:
                for val in xrange(y1, y2,-1):
                    self.expansion.append([x1, val])

    def get_target(self):
        """ this is for Part 2 """
        found = []
        for coord in self.expansion:
            if coord in found:
                self.target = coord
                return

            found.append(coord)

        self.target = None, None

    def calculate(self):
        """ this is for Part 1 """
        self.calc = self.moves.copy()
        out = []

        for key in self.moves:
            self.calc[key] = sum(self.calc[key])
            out.append(abs(self.calc[key]))

        return sum(out)

    def calc_coord(self, spaces):
        """
        self.degress=0, 90, 180, 270
        """
        current_coord = self.coords[-1]

        new_coord = current_coord[:]

        if self.degrees == 0:
            new_coord[1] = current_coord[1] + spaces

        if self.degrees == 180:
            new_coord[1] = current_coord[1] - spaces

        if self.degrees == 90:
            new_coord[0] = current_coord[0] + spaces

        if self.degrees == 270:
            new_coord[0] = current_coord[0] - spaces

        return new_coord

    def build_dict(self):
        for val in self.data:
            direction, spaces = val[0], int(val[1:])
            self.val = direction, spaces

            """ sets degrees correctly """
            self.turn(direction)

            """ now we can calc coords """
            coord = self.calc_coord(spaces)
            self.coords.append(coord)

            self.update_moves(spaces)

    def update_moves(self, spaces):
            """ updates where we have been """
            if self.degrees in (0, 90):
                self.moves[self.degrees].append(spaces)

            if self.degrees == 180:
                self.moves[0].append(-spaces)

            if self.degrees == 270:
                self.moves[90].append(-spaces)

    def read_file(self):
        alist = open(self.fqn, 'r').read().split(',')
        return [x.strip() for x in alist]

    def __str__(self):
        lookup = {0: 'North',
                  90: 'East',
                  180: 'South',
                  270: 'West',
                  }

        self.facing = lookup.get(self.degrees, None)
        return '<%s> degrees: %s facing: %s (%s)' % (self.__class__.__name__, self.degrees, self.facing, self.val)

    def turn(self, direction):
        """
        calculate facing
        """
        if direction == 'R':
            self.degrees = self.degrees + 90

        if direction == 'L':
            self.degrees = self.degrees - 90

        """ convert degrees """
        if self.degrees < 0:
            self.degrees = self.degrees + 360

        if self.degrees > 360:
            self.degrees = self.degrees - 360

        if self.degrees == 360:
            self.degrees = 0

if __name__ == '__main__':
    fqn = 'one_data.txt'
    vector = Vector(fqn)
    worked = vector()

    logger.info('You are %s blocks away from Easter Bunny HQ.' % (worked.blocks))
    logger.info('You are %s new blocks away from Easter Bunny HQ.' % (worked.new_blocks))
