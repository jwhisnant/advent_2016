#!/usr/bin/env python
# -*- coding: utf-8 -*-

"""
File: test_one.py
Author: jwhisnant
Email: jwhisnant@email.com
Github: https://github.com/jwhisnant
Description:


Following R2, L3 leaves you 2 blocks East and 3 blocks North, or 5 blocks away.
R2, R2, R2 leaves you 2 blocks due South of your starting position, which is 2 blocks away.
R5, L5, R5, R3 leaves you 12 blocks away.
"""

from one import Vector


def test_1():
    case_data = (['R2', 'L3'])
    case = Vector(data=case_data)()
    assert case.blocks == 5

    assert case.coords[1:] == [[2, 0], [2, 3]]


def test_2():
    case_data = ['R2', 'R2', 'R2']
    case = Vector(data=case_data)()
    assert case.blocks == 2

    assert case.coords[1:] == [[2, 0], [2, -2], [0, -2]]


def test_3():
    case_data = ['R5', 'L5', 'R5', 'R3']
    case = Vector(data=case_data)()

    assert case.blocks == 12
    assert case.coords[1:] == [[5, 0], [5, 5], [10, 5], [10, 2]]


def test_4():
    case_data = ['R100', 'R100', 'R100', 'R100']
    case = Vector(data=case_data)()

    assert case.blocks == 0
    assert case.coords[1:] == [[100, 0], [100, -100], [0, -100], [0, 0]]


def test_5():
    case_data = ['R0', 'L0']
    case = Vector(data=case_data)()

    assert case.degrees == 0

    assert case.blocks == 0
    assert case.coords[1:] == [[0, 0], [0, 0]]


def test_6():
    case_data = ['R100', 'L100']
    case = Vector(data=case_data)()

    assert case.blocks == 200
    assert case.coords[1:] == [[100, 0], [100, 100]]


def part_2():
    case_data = ['R8', 'R4', 'R4', 'R8']
    case = Vector(data=case_data)()

    assert case.new_blocks == 4


