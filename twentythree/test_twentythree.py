#!/usr/bin/env python
# -*- coding: utf-8 -*-

"""
File: test_twentythree.py
Author: yourname
Email: yourname@email.com
Github: https://github.com/yourname
Description:
"""

from twentythree import cpy, jnz, inc, dec, runner, tgl, two, three


# logging
import logging
logging.basicConfig(format='%(asctime)s:%(levelname)s:%(name)s:%(message)s')
logger = logging.getLogger(__name__)
logger.setLevel(logging.DEBUG)


def testtwo():
    """ inc unless inc, then dec """
    x = 'inc foo'
    assert two(x) == 'dec foo'

    x = 'dec bar'
    assert two(x) == 'inc bar'

    x = 'nonexistant dumb'
    assert two(x) == 'inc dumb'


def testthree():
    """ jnz, unles jnz, then cpy """
    x = 'jnz x y'
    assert three(x) == 'cpy x y'

    x = 'cpy x y'
    assert three(x) == 'jnz x y'

    x = 'stupid dumb silly'
    assert three(x) == 'jnz dumb silly'


def test_two_inc():

    registers = {'a': 1}
    instructions = ['tgl a', 'inc a']
    ins = 'tgl a'
    results = None

    later, registers, results = tgl(ins, registers, instructions, pos=0)
    assert later == 1
    assert registers == {'a': 1}
    assert results == ['tgl a', 'dec a']


def test_two_dec():

    registers = {'a': 1}
    instructions = ['tgl a', 'dec a']

    ins = 'tgl a'

    later, registers, results = tgl(ins, registers, instructions, pos=0)
    assert later == 1
    assert registers == {'a': 1}
    assert results == ['tgl a', 'inc a']


def test_two_tgl():
    registers = {'a': 1}
    instructions = ['tgl a', 'tgl a']
    ins = 'tgl a'

    later, registers, results = tgl(ins, registers, instructions, pos=0)

    assert later == 1
    assert registers == {'a': 1}
    assert results == ['tgl a', 'inc a']


def test_two_any():
    registers = {'a': 1}
    instructions = ['tgl a', 'silly foobar']
    ins = 'tgl a'

    later, registers, results = tgl(ins, registers, instructions, pos=0)

    assert later == 1
    assert registers == {'a': 1}
    assert results == ['tgl a', 'inc foobar']


def test_three_jnz():

    registers = {'a': 1}
    instructions = ['tgl a', 'jnz x y']
    ins = 'tgl a'

    later, registers, results = tgl(ins, registers, instructions, pos=0)
    assert later == 1
    assert registers == {'a': 1}
    assert results == ['tgl a', 'cpy x y']


def test_three_cpy():

    registers = {'a': 1}
    instructions = ['tgl a', 'cpy x y']
    ins = 'tgl a'
    later, registers, results = tgl(ins, registers, instructions, pos=0)

    assert later == 1
    assert registers == {'a': 1}
    assert results == ['tgl a', 'jnz x y']


def test_three_any():
    registers = {'a': 1}
    instructions = ['tgl a', 'silly dumb bunny']
    ins = 'tgl a'
    later, registers, results = tgl(ins, registers, instructions, pos=0)

    assert later == 1
    assert registers == {'a': 1}
    assert results == ['tgl a', 'jnz dumb bunny']


def test_part1():
    instructions = [
        'cpy 2 a',
        'tgl a',
        'tgl a',
        'tgl a',
        'cpy 1 a',
        'dec a',
        'dec a',
    ]

    registers = {'a': 0,
                 'b': 0,
                 'c': 0,
                 'd': 0,
                 }

    dispatch = {'cpy': cpy,
                'inc': inc,
                'dec': dec,
                'jnz': jnz,
                'tgl': tgl,
                }

    registers, states = runner(instructions, registers, dispatch)

    assert registers['a'] == 3
