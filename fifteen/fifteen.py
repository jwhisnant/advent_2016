#!/usr/bin/env python
# -*- coding: utf-8 -*-

"""
File: fifteen.py
Author: yourname
Email: yourname@email.com
Github: https://github.com/yourname
Description:
"""
from tqdm import tqdm

from collections import deque

# logging
import logging
logging.basicConfig(format='%(asctime)s:%(levelname)s:%(name)s:%(message)s')
logger = logging.getLogger(__name__)
logger.setLevel(logging.DEBUG)


def parse_line(line):
    line = line.strip()
    if line:
        temp = line.split()
        num = temp[1].replace('#', '')
        length = temp[3]

        time_data = temp[6]
        when = time_data.split('=')[-1].replace(',', '')

        pos = temp[-1].replace('.', '')

    return int(num), int(length), int(when), int(pos)


def parse_file(data):
    out = []
    for line in data:
        out.append(parse_line(line))
    return out


class Disc(object):

    """ Disc is on back of turtle. R.I.P. Pratchett """

    def __init__(self,
                 num,
                 when,
                 length,
                 pos,
                 cue=None,
                 advance=1,
                 ):

        self.num = num
        self.when = when
        self.length = length
        self.pos = pos
        self.cue = cue
        self.advance = advance

        if not self.cue:
            self.cue = deque([0] * self.length)
            self.cue[pos] = 1

        # XXX TODO: autocreate based on cue, maybe ... 2016/12/20  (jwhisnant)
    def __call__(self):
        return self

    def __repr__(self):
        return '<%s: %s>' % (self.__class__.__name__, repr(vars(self)))

    def rotate(self, num):
        self.cue.rotate(num)


class Puzzle(object):

    """ Disc container """

    def __init__(self,
                 discs=None,
                 hole=0,
                 rotations=1,
                 turns=0,
                 when=None,
                 ):

        self.discs = discs
        self.turns = turns
        self.when = when

        self.finished = False

        self._orig_discs = discs[:]

    def __call__(self):
        self.advance_discs()

        if self.check_all_one():
            self.finished = True
            return self

        return self

    def advance_discs(self):
        """ because ball falls slowly, each disk rotates more, starting at t=1 at disc 1 """
        for num, disc in enumerate(self.discs):
            self.discs[num].rotate(num + 1)
        self.turns = self.turns + 1

    def __repr__(self):
        return '<%s: %s>' % (self.__class__.__name__, repr(vars(self)))

    def check_all_one(self):
        """ can the ball drop through? """
        out = []
        for disc in self.discs:
            out.append(disc.cue[0])

        if all(out):
            return True

        return False


def test_part1():
    data = [
        'Disc #1 has 5 positions; at time=0, it is at position 4.',
        'Disc #2 has 2 positions; at time=0, it is at position 1',
    ]

    setup = parse_file(data)

    disks = []
    for num, length, when, pos in setup:
        disk = Disc(num=num, length=length, when=when, pos=pos)
        disks.append(disk)

    puzzle = Puzzle(discs=disks, rotations=20)
    worked = puzzle()
    assert puzzle.turns == 5


def advance_disks(disks, num=1):
    out = []
    for disc in disks:
        disc.rotate(num)
        out.append(disc)
    return out[:]


def orig_disks(data):
    """ hacky, but mutability is not fun """
    out = []
    for num, length, when, pos in parse_file(data):
        disk = Disc(num=num, length=length, when=when, pos=pos)
        out.append(disk)
    return out


def part1(given, how_many,step=1):
    data = parse_file(given)

    for x in tqdm(range(0, how_many)):
        out = orig_disks(given)
        out = advance_disks(out, x)
        # logger.info(out)

        puzzle = Puzzle(discs=out[:])
        worked = puzzle()

        if worked.finished:
            msg = '%s %s' % (x, worked)
            logger.info(msg)
            return worked


if __name__ == '__main__':
    """ part1 """
    given = open('fifteen_data.txt', 'r').readlines()
    worked = part1(given=given, how_many=1000 * 1000)
    #2016-12-20 16:00:04,110:INFO:__main__:148737 <Puzzle: {'turns': 1, ...

    """ part2 """
    given = open('fifteen_data.txt', 'r').readlines()
    given.append("Disc #7 has 11 positions; at time=0, it is at position 0.\n")
    worked = part1(given=given, how_many=1000 * 1000 * 100)

    # | 2352887/100000000 [03:41<2:32:44, 10655.43it/s]
    
    # 2016-12-20 16:11:17,689:INFO:__main__:2353212 <Puzzle: {'turns': 1, 'finished': True, 'discs': 

    # [<Disc: {'advance': 1, 'when': 0, 'pos': 2, 'length': 5, 'num': 1, 'cue': deque([1, 0, 0, 0, 0])}>, 
            # <Disc: {'advance': 1, 'when': 0, 'pos': 7, 'length': 13, 'num': 2, 'cue': deque([1, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0])}>, 
            # <Disc: {'advance': 1, 'when': 0, 'pos': 10, 'length': 17, 'num': 3, 'cue': deque([1, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0])}>, 
            # <Disc: {'advance': 1, 'when': 0, 'pos': 2, 'length': 3, 'num': 4, 'cue': deque([1, 0, 0])}>, 
            # <Disc: {'advance': 1, 'when': 0, 'pos': 9, 'length': 19, 'num': 5, 'cue': deque([1, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0])}>, 
            # <Disc: {'advance': 1, 'when': 0, 'pos': 0, 'length': 7, 'num': 6, 'cue': deque([1, 0, 0, 0, 0, 0, 0])}>, 
            # <Disc: {'advance': 1, 'when': 0, 'pos': 0, 'length': 11, 'num': 7, 'cue': deque([1, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0])}>], 
    # 'when': None, '_orig_discs': [<Disc: {'advance': 1, 'when': 0, 'pos': 2, 'length': 5, 'num': 1, 'cue': deque([1, 0, 0, 0, 0])}>, 
            # <Disc: {'advance': 1, 'when': 0, 'pos': 7, 'length': 13, 'num': 2, 'cue': deque([1, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0])}>, 
            # <Disc: {'advance': 1, 'when': 0, 'pos': 10, 'length': 17, 'num': 3, 'cue': deque([1, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0])}>, 
            # <Disc: {'advance': 1, 'when': 0, 'pos': 2, 'length': 3, 'num': 4, 'cue': deque([1, 0, 0])}>, 
            # <Disc: {'advance': 1, 'when': 0, 'pos': 9, 'length': 19, 'num': 5, 'cue': deque([1, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0])}>, 
            # <Disc: {'advance': 1, 'when': 0, 'pos': 0, 'length': 7, 'num': 6, 'cue': deque([1, 0, 0, 0, 0, 0, 0])}>, 
            # <Disc: {'advance': 1, 'when': 0, 'pos': 0, 'length': 11, 'num': 7, 'cue': deque([1, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0])}>]}>
    

