#!/usr/bin/env python
# -*- coding: utf-8 -*-

"""
File: test_fifteen.py
Author: yourname
Email: yourname@email.com
Github: https://github.com/yourname
Description:
"""
from fifteen import (
    parse_line,
    parse_file,
    Disc,
    Puzzle,
    advance_disks,
    orig_disks,
)

# logging
import logging
logging.basicConfig(format='%(asctime)s:%(levelname)s:%(name)s:%(message)s')
logger = logging.getLogger(__name__)
logger.setLevel(logging.DEBUG)

def _test_advance_disks():
    pass

def test_parse_line():
    line = 'Disc #1 has 5 positions; at time=0, it is at position 2.'
    assert parse_line(line) == (1, 5, 0, 2)


def test_parse_file():
    line = 'Disc #1 has 5 positions; at time=0, it is at position 2.'
    data = [line, line]
    assert len(data) == 2

def test_disc():
    disk=Disc(num=1,length=5,when=0, pos=4)
    assert list(disk.cue) ==[0, 0, 0, 0, 1]
    disk.rotate(1)
    assert disk.cue[0]==1

def test_puzzle():
    disk=Disc(num=1,length=5,when=0, pos=4)
    puzzle=Puzzle(discs= [  disk ] )

    worked=puzzle()
    assert worked.turns==1

def test_part1():
    data = [
        'Disc #1 has 5 positions; at time=0, it is at position 4.',
        'Disc #2 has 2 positions; at time=0, it is at position 1',
    ]


    for x in range(0,10):
        out=orig_disks(data)
        out=advance_disks(out,x)
        # logger.info(out)

        puzzle = Puzzle(discs=out[:])
        worked = puzzle()

        if worked.finished:
            msg='%s %s'%(x, worked)
            # logger.info(msg)

        if puzzle.finished:
            assert x == 5

if __name__=='__main__':
    test_part1()



