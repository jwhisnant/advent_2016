#!/usr/bin/env python
# -*- coding: utf-8 -*-

"""
File: test_eight.py
Author: yourname
Email: yourname@email.com
Github: https://github.com/yourname
Description:


    rect AxB turns on all of the pixels in a
        rectangle at the top-left of the screen which is A wide and B tall.

    rotate row y=A by B shifts all of the pixels in row A
        (0 is the top row) right by B pixels.
        Pixels that would fall off the right end appear at the left end of the row.

    rotate column x=A by B shifts all of the pixels in column A (0 is the left column)
    down by B pixels.
    Pixels that would fall off the bottom appear at the top of the column.

"""
from eight import Screen
from eight import (
    line2list,
    list2line,
)


def test_sanity():
    """TODO: Docstring for test_linear.
    Returns
    -------
    TODO

    """
    screen = Screen(2, 2, state='....')
    assert (screen.wide * screen.tall) == len(screen.state)


def test_transforms():
    """ test converting to lines and lists
    """
    starting = 'abcdefgh'
    width = 4
    val = line2list(starting, width)
    desired = ('abcd', 'efgh')

    assert(val) == desired
    assert(len(desired)) == len(starting) / width
    assert list2line(desired) == starting

    width = 2
    val = line2list(starting, width)
    desired = ('ab', 'cd', 'ef', 'gh')
    assert(val) == desired
    assert(len(desired)) == len(starting) / width
    assert list2line(desired) == starting


def test_rect():
    start = '.' * 7 * 3
    screen = Screen(wide=7, tall=3, state=start)
    positions = screen.calc_rect(a=3, b=2)
    screen.fill(positions)

    assert screen.state == '###....###...........'
