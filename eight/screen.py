#!/usr/bin/env python
# -*- coding: utf-8 -*-

"""
File: pixel.py
Author: yourname
Email: yourname@email.com
Github: https://github.com/yourname
Description:
    simple pixel class
"""

import string
import itertools

from collections import deque
from tqdm import tqdm

# logging
import logging
logging.basicConfig(format='%(asctime)s:%(levelname)s:%(name)s:%(message)s')
logger = logging.getLogger(__name__)
logger.setLevel(logging.DEBUG)


class Parser(object):

    def __init__(self,
                 fqn='eight_data.txt',
                 instructions=None,
                 ):

        self.fqn = fqn
        self.instructions = instructions
        if not self.instructions:
            self.instructions = []

    def __call__(self):
        data = open(self.fqn, 'r')
        for line in data:
            self.line = line.strip()
            task = self.parse()
            self.instructions.append(task)
        return self

    def parse(self):
        if 'rect' in self.line:
            return self._parse_rect()

        if 'rotate' in self.line:
            return self._parse_rotate_row()

    def _parse_rect(self):
        temp = self.line.split()
        parts = temp[1]
        pieces = parts.split('x')
        return ('rect', int(pieces[0]), int(pieces[1]))

    def _parse_rotate_row(self):
        temp = self.line.split()

        word = temp[1]
        coord = temp[2]

        bar = coord.split('=')
        return (word, int(bar[-1]), int(temp[-1]))


class Pixel(object):

    def __init__(self,
                 broken=False,
                 on=False,
                 on_char='#',
                 off_char='.',
                 broken_char=''):

            self.broken = broken
            self.on = on
            self.on_char = on_char
            self.off_char = off_char
            self.broken_char = broken_char

    @property
    def show(self):

        if self.broken:
            return self.broken_char

        if self.on:
            return self.on_char

        if not self.on:
            return self.off_char

    def __repr__(self):
        return "<%s: show:'%s' on:%s broken:%s>" % (self.__class__.__name__,  self.show, self.on, self.broken)

    @property
    def turn_on(self):
        self.on = True

    @property
    def turn_off(self):
        self.on = False

    @property
    def smash(self):
        self.broken = True

    @property
    def fix(self):
        self.broken = False

    @property
    def toggle(self):
        if self.on:
            self.on = False
            return self.on

        if not self.on:
            self.on = True
            return self.on


class Row(object):

    def __init__(self, length, chars='#'):
        """ define row of pixels """
        self.pixels = deque()

        how_many = range(0, length)

        text = itertools.cycle(chars)

        for pos in how_many:
            #self.pixels.append(Pixel(on=True, on_char=chr(97+x) ) )
            self.pixels.append(Pixel(on_char=text.next()))

    def __len__(self):
        return len(self.pixels)

    @property
    def show(self):
        val = ''.join([pix.show for pix in self.pixels])
        return val

    def __repr__(self):
        return "<%s: '%s' >" % (self.__class__.__name__,  self.show)

    @property
    def toggle(self):
        for pix in self.pixels:
            pix.toggle

    @property
    def turn_on(self):
        for pix in self.pixels:
            pix.turn_on

    @property
    def turn_off(self):
        for pix in self.pixels:
            pix.turn_off

    def move_right(self, number):
        self.pixels.rotate(number)

    def move_left(self, number):
        self.pixels.rotate(-number)


def column_down(column, by):
    """ this is equivalent to 'down' """
    column = deque(tuple(column))
    new_row = Row(len(column))
    del new_row.pixels
    new_row.pixels = deque(column)
    new_row.move_right(by)
    to_apply = [pixel.show for pixel in new_row.pixels]
    return to_apply


class Screen(object):

    def __init__(self,
                 wide=None,
                 tall=None,
                 rows=None,
                 step=0,
                 parser=None,
                 ):

        self.wide = wide
        self.tall = tall
        self.rows = rows
        self.step = step

        self.parser = parser
        if not self.parser:
            self.parser = Parser()

        self.steps = []

        # XXX TODO: argument checking 2016/12/13  (james)
        if not self.rows:
            self.rows = deque()

            for x in range(0, self.tall):
                self.rows.append(Row(self.wide))

        if self.rows:
            self.wide = len(self.rows[0])
            self.tall = len(self.rows)

    def __call__(self):
        parsed = self.parser()
        for task in parsed.instructions:
            what, arg1, arg2 = task

            if what == 'rect':
                self.rect(arg1, arg2)

            if what == 'row':
                self.rotate_row(arg1, arg2)

            if what == 'column':
                self.rotate_column(arg1, arg2)

        return self

    def bare_repr(self):
        val = '\n'.join([row.show for row in self.rows])
        return val

    def replay(self):
        for bare, full in self.steps:
            print full
            raw_input("Press Enter to continue...")

    def __repr__(self):
        val = self.bare_repr()
        if self.parser.instructions:
            try:
                task = self.parser.instructions[self.step]
                task = '<instructions: %s>' % (repr(task))

            except IndexError:
                task = '<instructions: unknown>'

        return """
<%s: wide:%s tall:%s step:%s>
%s
%s
""" % (self.__class__.__name__,  self.wide, self.tall, self.step, task, val)

# XXX TODO: decorator 2016/12/13  (jwhisnant)
    def advance(self):
        self.steps.append((self.bare_repr(), self.__repr__()))
        self.step = self.step + 1

    def rotate_row(self, y, by):
        target = self.rows[y]
        target.move_right(by)
        self.advance()

    def rect(self, a, b):
        how_many = range(0, b)
        for i in how_many:
            for j in range(0, a):
                self.rows[i].pixels[j].turn_on
        self.advance()

    def rotate_column(self, where, by):
        out = []
        for num, row in enumerate(self.rows):
            out.append(row.pixels[where])

        chars = column_down(out, by)
        self.apply(where, chars)

        self.advance()

    def apply(self, where, chars):
        for num, row in enumerate(self.rows):
            pixel = row.pixels[where]
            new_char = chars[num]

            if new_char == '#':
                pixel.on = True
                pixel.on_char = new_char

            if new_char == '.':
                pixel.on = False

if __name__ == '__main__':
    screen = Screen(wide=50, tall=6)
    worked = screen()

    # 52 is too low ... parse_rect only first char getting
    #90 is also too low ...
    #110 is the theoretical max by counting all the rect's - and that is the desired response ...

    """ part 1 """
    foo = []
    for word, a, b in worked.parser.instructions:
        if word == 'rect':
            foo.append(a * b)
    res = sum(foo)
    logger.info('Number of LEDs that would light: %s' % (res))

    """part 2 ??? """
    print worked
