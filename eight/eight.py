#!/usr/bin/env python
# -*- coding: utf-8 -*-

"""
File: eight.py
Author: yourname
Email: yourname@email.com
Github: https://github.com/yourname
Description:
    http://adventofcode.com/2016/day/8
"""


class Screen(object):

    def __init__(self, wide=50, tall=6):
        """TODO: Docstring for __init__.

        Parameters
        ----------
        arg1 : TODO
        """
        self.wide = wide
        self.tall = tall

    def __call__(self, arg1):
        """TODO: Docstring for __call__.

        Parameters
        ----------
        arg1 : TODO

        Returns
        -------
        TODO

        """
        pass

    def rect(self, a, b):
        """TODO: Docstring for rect.

        Parameters
        ----------
        a : int
        b : int

        Returns
        -------
        TODO

        """
        pass

    def rotate(self, arg1):
        """TODO: Docstring for rotate.

        Parameters
        ----------
        arg1 : TODO

        Returns
        -------
        TODO

        """
        pass
