#!/usr/bin/env python
# -*- coding: utf-8 -*-

"""
File: test_screen.py
Author: yourname
Email: yourname@email.com
Github: https://github.com/yourname
Description:
"""

from screen import (
    Parser,
    Pixel,
    Row,
    Screen,
)


def test_parser():
    parser = Parser()

    data = (
        ('rect 1x1', ('rect', 1, 1)),
        ('rotate row y=0 by 2', ('row', 0, 2)),
        ('rotate column x=32 by 1', ('column', 32, 1)),
        ('rect 17x1', ('rect', 17, 1)),
        ('rotate row y=1 by 20', ('row', 1, 20)),
    )

    for line, intermediate in data:
        parser.line = line.strip()
        assert parser.parse() == intermediate


def test_pixel():
    pix = Pixel(broken=False, on=True, on_char='a', broken_char='Z')

    assert(pix.on) == True
    assert(pix.show) == 'a'

    pix.turn_off
    assert(pix.show) == '.'

    pix.smash
    assert(pix.broken) == True

    pix.toggle
    assert(pix.on) == True

    pix.toggle
    assert(pix.on) == False


def test_row():
    start = 'abcdefgh'
    row = Row(8, chars=start)

    """ default pixel is off """
    assert row.show == '.' * 8

    row.turn_on
    assert row.show == start

    row.turn_off
    assert row.show == '.' * 8

    row.toggle
    assert row.show == start

    row.toggle
    assert row.show == '.' * 8

    row.move_right(1)
    assert row.show == '.' * 8

    row.turn_on
    assert row.show == 'habcdefg'

    row.move_left(1)
    assert row.show == start


def test_screen():
    one = """
###....
###....
......."""

    two = """
#.#....
###....
.#....."""

    three = """
....#.#
###....
.#....."""

    four = """
.#..#.#
#.#....
.#....."""

    screen = Screen(wide=7, tall=3)
    screen.rect(a=3, b=2)
    assert(screen.bare_repr()) == one.strip()

    screen.rotate_column(where=1, by=1)
    assert(screen.bare_repr()) == two.strip()

    screen.rotate_row(y=0, by=4)
    assert(screen.bare_repr()) == three.strip()

    screen.rotate_column(where=1, by=1)
    assert(screen.bare_repr()) == four.strip()

    assert(screen.step) == 4
