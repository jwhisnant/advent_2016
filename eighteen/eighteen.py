#!/usr/bin/env python
# -*- coding: utf-8 -*-

"""
File: eighteen.py
Author: yourname
Email: yourname@email.com
Github: https://github.com/yourname
Description:
"""

from tqdm import tqdm

# logging
import logging
logging.basicConfig(format='%(asctime)s:%(levelname)s:%(name)s:%(message)s')
logger = logging.getLogger(__name__)
logger.setLevel(logging.DEBUG)


class Tile(object):

    """ I should have listened to General Ackbar """

    def __init__(self,
                 trapped=False,
                 ):

        self.trapped = trapped

    @property
    def char(self):
        if self.trapped:
            char = '^'
        if not self.trapped:
            char = '.'
        return char

    def __repr__(self):
        return '<%s: %s>' % (self.__class__.__name__, self.char)


class Row(object):

    """ number of tiles """

    def __init__(self,
                 tiles=None,
                 chars=''):

        self.tiles = tiles
        self.chars = chars

        if not self.tiles:
            if self.chars:
                temp = []
                for char in chars:
                    if char == '.':
                        atile = Tile(trapped=False)
                    if char == '^':
                        atile = Tile(trapped=True)
                    temp.append(atile)

            self.tiles = list(temp)

    @property
    def disp(self):
        disp = ''.join([x.char for x in self.tiles])
        return disp

    def __repr__(self):

        return """
<%s: %s>

%s""" % (self.__class__.__name__, len(self.tiles), self.disp)


class Grid(object):

    """ series of rows """

    def __init__(self,
                 rows=None):

        self.rows = rows
        if not self.rows:
            self.rows = []

    @property
    def disp(self):
        disp = '\n'.join([row.disp for row in self.rows])
        return disp

    def __repr__(self):
        return """
<%s: (%s)>

%s""" % (self.__class__.__name__,
         len(self.rows), self.disp)


class Logic(object):

    """ compare two rows """

    def __init__(self,
                 known=None,
                 unknown=None,
                 solved=None,
                 ):

        self.known = known
        self.unknown = unknown
        self.solved = solved

        """ autogen """
        if self.known and not self.unknown:
            row = Row(chars='.' * len(self.known.tiles))
            self.unknown = row

        """ make a copy """
        if not self.solved:
            self.solved = Row(tuple([tile for tile in self.unknown.tiles]))

    def __call__(self):
        """ determine how a row is trapped
        return unknown row
        """

        for num, tile in enumerate(self.unknown.tiles):
            left, center, right = self.get_adjacent(num)
            rules = (self._one(left, center, right),
                     self._two(left, center, right),
                     self._three(left, center, right),
                     self._four(left, center, right),
                     )
            if any(rules):
                self.solved.tiles[num].trapped = True

        return self

    def get_adjacent(self, num):
        center_tile = self.known.tiles[num] # this always works ...

        end = len(self.unknown.tiles) - 1

        """ assumes a size """
        if num != 0:
            left_tile = self.known.tiles[num - 1] # this always works ...
        if num == 0:
            left_tile = Tile(trapped=False)

        if num != end:
            right_tile = self.known.tiles[num + 1] # this always works ...
        if num == end:
            right_tile = Tile(trapped=False)

        return left_tile, center_tile, right_tile

    def _one(self, left, center, right):
        """ doc """
        if left.trapped and center.trapped:
            if not right.trapped:
                return 1
        return 0

    def _two(self, left, center, right):
        """ doc """
        if right.trapped and center.trapped:
            if not left.trapped:
                return 1
        return 0

    def _three(self, left, center, right):
        """ doc """
        if left.trapped:
            if not center.trapped:
                if not right.trapped:
                    return 1
        return 0

    def _four(self, left, center, right):
        """ doc """
        if right.trapped:
            if not center.trapped:
                if not left.trapped:
                    return 1
        return 0


def part1(data, how_many, show=True):
    count = 0
    known_row = Row(chars=data)
    grid = Grid(rows=[known_row])

    count = count + known_row.disp.count('.')

    # if show:
        # print 0, known_row.disp

    for x in tqdm(range(1, how_many)):
        logic = Logic(known=known_row)

        worked = logic()

        # if show:
            # print x, worked.solved.disp

        count = count + worked.solved.disp.count('.')

        known_row = worked.solved

        # update grid
        if show:
            grid.rows.append(worked.solved)

    if show:
        print grid.disp

    msg = '%s safe squares' % (count)
    logger.info(msg)
    return count

if __name__ == '__main__':
    data = open('eighteen_data.txt', 'r').read().strip()

    """ part1 """
    grid = part1(data, 40, show=True)
    #2016-12-20 12:51:00,470:INFO:__main__:1961 safe squares

    """ part2,srsly? """
    grid=part1(data,400000,show=False)
    # [03:43<00:00, 1787.17it/s] (later) ...
    #2016-12-20 12:55:29,950:INFO:__main__:20000795 safe squares


