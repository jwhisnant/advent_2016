#!/usr/bin/env python
# -*- coding: utf-8 -*-

"""
File: test_eighteen.py
Author: yourname
Email: yourname@email.com
Github: https://github.com/yourname
Description:
"""

from eighteen import Row, Grid, Tile, Logic

# logging
import logging
logging.basicConfig(format='%(asctime)s:%(levelname)s:%(name)s:%(message)s')
logger = logging.getLogger(__name__)
logger.setLevel(logging.DEBUG)


def test_part1_1():

    known_row = Row(tiles=(Tile(trapped=False),
                           Tile(trapped=False),
                    Tile(trapped=True),
                    Tile(trapped=True),
                    Tile(trapped=False), )
                    )

    grid = Grid(rows=[known_row])

    for x in range(1, 3):
        unknown_row = Row(tiles=(Tile(trapped=False),
                                 Tile(trapped=False),
                          Tile(trapped=False),
                          Tile(trapped=False),
                          Tile(trapped=False), )
                          )

        logic = Logic(
            known=known_row,
            unknown=unknown_row,
        )

        worked = logic()
        known_row = worked.solved

        # update grid
        grid.rows.append(worked.solved)

    disp = grid.disp
    expected = """
..^^.
.^^^^
^^..^
"""
    assert disp == expected.strip()

def test_part1_2():
    '.^^.^.^^^^'
    known_row = Row(tiles=(Tile(trapped=False),
                           Tile(trapped=True),
                    Tile(trapped=True),
                    Tile(trapped=False),
                    Tile(trapped=True), 
                    Tile(trapped=False), 
                    Tile(trapped=True), 
                    Tile(trapped=True), 
                    Tile(trapped=True), 
                    Tile(trapped=True), 
                    )
                    )

    grid = Grid(rows=[known_row])

    for x in range(1, 10):
        unknown_row = Row(tiles=(Tile(trapped=False),
                                 Tile(trapped=False),
                          Tile(trapped=False),
                          Tile(trapped=False),
                          Tile(trapped=False),
                          Tile(trapped=False),
                          Tile(trapped=False),
                          Tile(trapped=False),
                          Tile(trapped=False),
                          Tile(trapped=False), )
                          )

        logic = Logic(
            known=known_row,
            unknown=unknown_row,
        )

        worked = logic()
        known_row = worked.solved

        # update grid
        grid.rows.append(worked.solved)

    disp = grid.disp
    expected = """
.^^.^.^^^^
^^^...^..^
^.^^.^.^^.
..^^...^^^
.^^^^.^^.^
^^..^.^^..
^^^^..^^^.
^..^^^^.^^
.^^^..^.^^
^^.^^^..^^
"""
    assert disp == expected.strip()

if __name__=='__main__':
    pass

