#!/usr/bin/env python
# -*- coding: utf-8 -*-

"""
File: twelve.py
Author: yourname
Email: yourname@email.com
Github: https://github.com/yourname
Description: 
"""

# logging
import logging
logging.basicConfig(format='%(asctime)s:%(levelname)s:%(name)s:%(message)s')
logger = logging.getLogger(__name__)
logger.setLevel(logging.DEBUG)

from pprint import pprint as pp
from tqdm import tqdm

def cpy(ins,registers):
    """
    return the next func location
    """
    temp=ins.split()
    x=temp[1]
    y=temp[2]

    if x in registers:
        x=registers[x]

    registers[y]=int(x)
    return 1,registers

def inc(ins,registers):
    """
    return the next func location
    """

    temp=ins.split()
    x=temp[1]
    registers[x]=registers[x]+1

    return 1,registers

def dec(ins,registers):
    """
    return the next func location
    """

    temp=ins.split()
    x=temp[1]
    registers[x]=registers[x]-1

    return 1,registers

def jnz(ins,registers):
    """
    return the next func location
    """
    temp=ins.split()
    x=temp[1]
    y=temp[2]

    if x not in registers:
        if int(x):
            return int(y), registers

    if x in registers:
        val=registers[x]
        if val:
            return int(y), registers

    return 1,registers


def runner(instructions,registers,dispatch, save_state=False):
    _instructions=tuple(instructions)

    pos=0
    count=0
    states=[]

    with tqdm() as pbar:
        while pos<=len(instructions):
            # logger.info('pos now: %s'%(pos) )
            try:
                ins=instructions[pos]
            except IndexError as err:
                # XXX TODO: BunnyHopException 2016/12/15  (james)
                logger.info('end of program')
                # logger.exception(err)
                val="(%s) (%s) pos:%s next:%s, ins:'%s' reg:%s"%(len(instructions), count, pos,pos+res,ins,registers.copy())
                logger.info(val)
                return registers,states

            name=ins.split()[0]
            func=dispatch[name]
            res,registers=func(ins,registers)
            pos=pos+res

            val="(%s) (%s) pos:%s next:%s, ins:'%s' reg:%s"%(len(instructions), count, pos,pos+res,ins,registers.copy())
            # logger.info(val)
            if save_state:
                states.append( (registers, val) )

            if pos<0: #under bounds
                pos=0

            if pos==len(instructions):
                pass
                """ idc """

            if pos>len(instructions):
                """ idc """
                logger.info(val)
                return registers,states

            count=count+1
            pbar.update(count)


    return registers,states

def run_part1(save_state=False):
    instructions=open('twelve_data.txt','r').readlines()

    _instructions=tuple(instructions)
    dispatch={'cpy':cpy,
            'inc':inc,
            'dec':dec,
            'jnz':jnz,
            }

    registers={'a':0,
            'b':0,
            'c':0,
            'd':0,
            }


    cleaned=[x.strip() for x in instructions if x.strip()]

    return runner(cleaned,registers,dispatch,save_state)


def run_part2(save_state=False):
    """ as in part 1, only c=1 to start """
    instructions=open('twelve_data.txt','r').readlines()

    _instructions=tuple(instructions)
    dispatch={'cpy':cpy,
            'inc':inc,
            'dec':dec,
            'jnz':jnz,
            }

    registers={'a':0,
            'b':0,
            'c':1,
            'd':0,
            }


    cleaned=[x.strip() for x in instructions if x.strip()]

    return runner(cleaned,registers,dispatch, save_state)
 
if __name__=='__main__':
    """ part 1 """
    # registers,states= run_part1(save_state=False)

    """ part 2 """
    registers,states= run_part2(save_state=False)

