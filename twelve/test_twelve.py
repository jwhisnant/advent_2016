#!/usr/bin/env python
# -*- coding: utf-8 -*-

"""
File: test_twelve.py
Author: yourname
Email: yourname@email.com
Github: https://github.com/yourname
Description: 
"""

from twelve import cpy, jnz, inc, dec, runner


# logging
import logging
logging.basicConfig(format='%(asctime)s:%(levelname)s:%(name)s:%(message)s')
logger = logging.getLogger(__name__)
logger.setLevel(logging.DEBUG)


def test_part1():
    instructions=[
    'cpy 41 a',
    'inc a',
    'inc a',
    'dec a',
    'jnz a 2',
    'dec a',
    ]

    registers={'a':0,
            'b':0,
            'c':0,
            'd':0,
            }

    dispatch={'cpy':cpy,
            'inc':inc,
            'dec':dec,
            'jnz':jnz,
            }

    registers,states=runner(instructions,registers,dispatch)

    assert registers['a']==42
