#!/usr/bin/env python
# -*- coding: utf-8 -*-

"""
File: fourteen.py
Author: yourname
Email: yourname@email.com
Github: https://github.com/yourname
Description:
"""

import hashlib
import string
from collections import deque
from tqdm import tqdm

# logging
import logging
logging.basicConfig(format='%(asctime)s:%(levelname)s:%(name)s:%(message)s')
logger = logging.getLogger(__name__)
logger.setLevel(logging.INFO)
#logger.setLevel(logging.DEBUG)


def is_repeated(data, length):
    """ return first found repeated
    """
    for num, char in enumerate(data):
        start = num
        end = num + length

        """ is repeated """
        test = data[start:end]
        find = data[num] * length
        # too_many = data[num] * length + 1

        if test == find:
            """ exactly this length is required"""
            # if not data.find(too_many):
            return test

    return False


def generate_key(salt, index):
    """ gen num of md5 keys """
    hasher = hashlib.new('md5')
    hasher.update(salt + str(index))
    val = hasher.hexdigest()
    return val


def generate_keys(salt, num, how_many, stretch=0):
    """ add more to the queue and return it """
    out = []
    with tqdm(total=how_many) as pbar:
        for x in range(num, how_many + num):
            key = generate_key(salt, x)

            if not stretch:
                out.append(key)

            if stretch:
                longer = key_stretch(key, number=stretch)
                out.append(longer)

            #count
            pbar.update()

    return out


def search_fifth(queue, lookfor, start):
    """ only give
    me a queue of the correct size to lookin in ...
    """
    for index, word in enumerate(queue, start=start):
        if lookfor in word:
            logger.debug('Found fifth: %s %s' % (index, word))
            return index, word

    return None, None


def _range(queue, start, end):
    out = []
    for x in xrange(start, end):
        out.append(queue[x])
    return out


def key_stretch(hashing, number=2016):
    """ meh """

    val = hashing

    for num in xrange(0, number):
        hasher = hashlib.new('md5')
        hasher.update(val)
        val = hasher.hexdigest()
    return val


def part1(
    salt='yjdafjpo',
    search_ahead=1000,
    required=64,
    pregen=30000,
    stretch=0,
):

    num = 0
    discarded = 0
    queue = deque()
    keys = []
    triples = []

    logger.info('Generating %s keys' % (pregen))
    more = generate_keys(salt, num, pregen, stretch=stretch)

    queue.extend(more)

    for num, possible in enumerate(queue):
        triple = is_repeated(possible, 3)

        if triple:
            triple_data = (num, triple, possible)
            # logger.debug('Found triple: %s %s' % (num, possible))
            rep = triple[0] * 5

            ahead = num + 1 + search_ahead

            small = _range(queue, num + 1, ahead)
            index, word = search_fifth(small, rep, start=num + 1)

            if index and word:
                fifth_data = (index, rep, queue[index])
                data = triple_data, fifth_data
                keys.append(data)
                logger.info('(%s) %s keys found, %s' % (index, len(keys), repr(data)))

        if len(keys) == required:
            logger.info('Found %s keys at index: %s' % (required, num))
            return 'done', keys, num

    return 'notdone', keys, num

if __name__ == '__main__':
    # salt = 'abc' # test
    salt='yjdafjpo'
    status, keys, num = part1(salt=salt)

    # 2016-12-19 12:48:38,776:INFO:__main__:Found 64 keys at index: 25427 - woot

    """ test """
    status, keys, num = part1(stretch=2016, salt=salt)
    #2016-12-19 14:04:56,193:INFO:__main__:Found 64 keys at index: 22045
