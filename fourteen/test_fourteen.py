#!/usr/bin/env python
# -*- coding: utf-8 -*-

"""
File: fourteen.py
Author: yourname
Email: yourname@email.com
Github: https://github.com/yourname
Description:
"""

import pytest

import hashlib
import json

from fourteen import (
    generate_key,
    generate_keys,
    is_repeated,
    _range,
    search_fifth,
    key_stretch,
    part1,
)


from collections import deque

# logging
import logging
logging.basicConfig(format='%(asctime)s:%(levelname)s:%(name)s:%(message)s')
logger = logging.getLogger(__name__)
logger.setLevel(logging.INFO)


def test_is_repeated():
    assert is_repeated('abcdeee', 3) == 'eee'
    assert is_repeated('abcdeee', 4) == False

    assert is_repeated('3aeeeee1367614f3061d165a5fe3cac3', 5) == 'eeeee'


def test_generate_key():
    assert '0034e0923cc38887a57bd7b1d4f953df' == generate_key('abc', 18)
    assert '347dac6ee8eeea4652c7476d0f97bee5' == generate_key('abc', 39)

    assert 'ae2e85dd75d63e916a525df95e999ea0' == generate_key('abc', 92)


def test_add_more():
    assert len(generate_keys('foo', 0, 8)) == 8
    assert len(generate_keys('foo', 90, 8)) == 8


def test_strectch():
    val = '577571be4de9dcce85a041ba0410f29f'
    assert key_stretch(val, number=2016) == 'a107ff634856bb300138cac6568c0f24'


def _test_part1():
    status, keys, num = part1(stretch=0, salt='abc')
    assert num == 22728

def test_part2():
    status, keys, num = part1(stretch=2016, salt='abc')
    assert num == 22551


# XXX FIXME: ugh! 2016/12/19  (jwhisnant)
# @pytest.mark.skip(reason="aint nobody got time for that!")
# def old_test_part1():
    # discarded = 0

    # queue = deque()
    # keys = []
    # triples = []

    # salt = 'abc'
    # num = 0
    # search_ahead = 1000
    # required = 64

    # pregen = 30000

    # logger.info('Generating %s keys' % (pregen))
    # more = generate_keys(salt, num, pregen)
    # queue.extend(more)

    # for num, possible in enumerate(queue):
        # triple = is_repeated(possible, 3)

        # if triple:
            # triple_data = (num, triple, possible)
            # # logger.debug('Found triple: %s %s' % (num, possible))
            # rep = triple[0] * 5

            # ahead = num + 1 + search_ahead

            # small = _range(queue, num + 1, ahead)
            # index, word = search_fifth(small, rep, start=num + 1)

            # if index and word:
                # fifth_data = (index, rep, queue[index])
                # data = triple_data, fifth_data
                # keys.append(data)
                # logger.info('%s keys found' % (len(keys)))

        # if len(keys) == required:
            # logger.info('Found %s keys at index: %s' % (required, num))
            # assert(num) == 22728
            # return keys, num

    # return None, None
