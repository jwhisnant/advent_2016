#!/usr/bin/env python
# -*- coding: utf-8 -*-
"""
File: three.py
Author: yourname
Email: yourname@email.com
Github: https://github.com/yourname
Description:
    too easy, there must be a trick
"""

from three import valid_triangle


def test1():
    seq = (25, 10, 5)
    assert valid_triangle(seq) == False


def test_main():
    seq = (25, 10, 5)
    adict = main(seq)
    assert len(adict.get(False) == 1)


def test_eq_false():
    seq = (3, 4, 7)
    adict = main(seq)
    assert len(adict.get(False) == 1)
