#!/usr/bin/env python
# -*- coding: utf-8 -*-
"""
File: three.py
Author: yourname
Email: yourname@email.com
Github: https://github.com/yourname
Description:
    too easy, there must be a trick
"""
import itertools


def read_file(fqn='three_data.txt'):
    return [x.strip().split() for x in open(fqn, 'r')]


def three(col):
    out = []
    some = []
    for item in col:
        some.append(item)
        if len(some) == 3:
            out.append(tuple(some))
            some = []
    return out


def read_file_vert(fqn='three_data.txt'):
    data = open(fqn, 'r').read()
    temp = data.split()

    row_one = temp[::3]
    row_two = temp[1::3]
    row_three = temp[2::3]

    return three(row_one) + three(row_two) + three(row_three)

def main(data, adict=None):
    if not adict:
        adict = {}

    for item in data:
        seq = tuple([int(x) for x in item])
        key = bool(valid_triangle(sorted(seq)))

        if key not in adict:
            adict[key] = []

        adict[key].append(seq)

    return adict


def valid_triangle(seq):
    for comb in itertools.combinations(seq, 3):
        one, two, three = comb
        if one + two < three:
            return False
        if one + two == three:
            return False

    return True


def part1():
    """ part1 """
    data = read_file()
    adict = main(data)
    print len(adict[True])

#2618 is too high
def part2():
    """ part 2 """
    data= read_file_vert()
    adict = main(data)
    print len(adict[True])

if __name__ == '__main__':
    #part1()
    part2()
