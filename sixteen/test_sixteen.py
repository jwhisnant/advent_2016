#!/usr/bin/env python
# -*- coding: utf-8 -*-

"""
File: test_sixteen.py
Author: yourname
Email: yourname@email.com
Github: https://github.com/yourname
Description: 
--- Day 16: Dragon Checksum ---

"""

from sixteen import (
        generate_data,
        checksum_one,
        checksum,
        fill,
        )

# logging
import logging
logging.basicConfig(format='%(asctime)s:%(levelname)s:%(name)s:%(message)s')
logger = logging.getLogger(__name__)
logger.setLevel(logging.DEBUG)



def test_generate_data():
    data={
    '1':'100',
    '0':'001',
    '11111':'11111000000',
    '111100001010':'1111000010100101011110000'
    }

    for incoming, expected in data.items():
        assert generate_data(incoming) == expected

def test_checksum_one():
    assert checksum_one('110010110100') =='110101'

def test_checksum():
    assert checksum('110010110100') == '100'

def test_fill():
    assert fill('10000', 20) == '01100'
