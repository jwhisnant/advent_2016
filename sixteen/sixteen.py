#!/usr/bin/env python
# -*- coding: utf-8 -*-

"""
File: sixteen.py
Author: yourname
Email: yourname@email.com
Github: https://github.com/yourname
Description:
--- Day 16: Dragon Checksum ---

"""

# logging
import logging
logging.basicConfig(format='%(asctime)s:%(levelname)s:%(name)s:%(message)s')
logger = logging.getLogger(__name__)
logger.setLevel(logging.DEBUG)

from tqdm import tqdm


def flip_bits(b):
    out = ''
    for char in b:
        if char == '0':
            out = out + '1'
        if char == '1':
            out = out + '0'
    return out


def generate_data(a):
    b = ''.join(reversed(a))
    b = flip_bits(b)

    return a + '0' + b


def checksum_one(data):
    output = []
    one = ('00', '11')
    zero = ('10', '01')

    pieces = zip(data[0::2], data[1::2])
    for piece in pieces:
        if ''.join(piece) in one:
            output.append('1')
        if ''.join(piece) in zero:
            output.append('0')
    return ''.join(output)


def checksum(data):
    result = checksum_one(data)
    while len(result) % 2 == 0:
        result = checksum_one(result)
    return result


def fill(initial, size):
    """
    initial - starting data
    size - length of disk
    """
    data = initial

    with tqdm() as pbar:
        while len(data) < size:
            data = generate_data(data)
            pbar.update()

    """ we have enough data, make shorter """
    needed = data[:size]

    """ checksum """
    hashed = checksum(needed)

    return hashed

if __name__ == '__main__':
    puzzle_data = '00111101111101000'
    """ part1 """
    size = 272
    #2016-12-20 09:29:23,449:INFO:__main__:Checksum: 10011010010010010

    """ part2 """
    size = 35651584
    #2016-12-20 09:37:07,754:INFO:__main__:Checksum: 10101011110100011

    hashed = fill(puzzle_data, size)
    logger.info('Checksum: %s' % (hashed))
