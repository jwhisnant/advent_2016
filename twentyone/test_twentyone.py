#!/usr/bin/env python
# -*- coding: utf-8 -*-

"""
File: test_twentyone.py
Author: yourname
Email: yourname@email.com
Github: https://github.com/yourname
Description:
"""

from collections import deque
from twentyone import (
    swap_index,
    swap_letters,
    rotate_string,
    rotate_based_on_char,
    reverse,
    move_position,
)

# logging
import logging
logging.basicConfig(format='%(asctime)s:%(levelname)s:%(name)s:%(message)s')
logger = logging.getLogger(__name__)
logger.setLevel(logging.DEBUG)


"""
 x swap position 4 with position 0 swaps the first and last letters, producing the input for the next step, ebcda.
 x swap letter d with letter b swaps the positions of d and b: edcba.
 x reverse positions 0 through 4 causes the entire string to be reversed, producing abcde.
 x rotate left 1 step shifts all letters left one position, causing the first letter to wrap to the end of the string: bcdea.
 x move position 1 to position 4 removes the letter at position 1 (c), then inserts it at position 4 (the end of the string): bdeac.
 x move position 3 to position 0 removes the letter at position 3 (a), then inserts it at position 0 (the front of the string): abdec.
 x rotate based on position of letter b finds the index of letter b (1), then rotates the string right once plus a number of times equal to that index (2): ecabd.

rotate based on position of letter d finds the index of letter d (4),
then rotates the string right once, plus a number of times equal to that index, 
plus an additional time because the index was at least 4, 
for a total of 6 right rotations: decab.


"""


def test_swap_index():
    """
    one=astring[a]
    two=astring[b]

    astring[b]=one
    astring[a]=two
    return astring
    """
    assert swap_index('abcde', 4, 0) == 'ebcda'


def test_swap_letters():
    """ we cheat, since "X" is not in our set of
    starting letters
    return astring.replace(x, "X").replace(y,x).replace("X",y)
    """
    val = swap_letters('ebcda', 'd', 'b') == 'edcba'


def test_rotate_string():
    """
    rotate left 1 step shifts all letters left one position, causing the first letter to wrap to the end of the string: bcdea.
    """

    val = rotate_string('abcde', 'left', 1)
    assert val == 'bcdea'


def test_rotate_based_on_char():
    """ huh?
    x move position 3 to position 0 removes the letter at position 3 (a), then inserts it at position 0 (the front of the string): abdec.
    x rotate based on position of letter b finds the index of letter b (1), then rotates the string right once plus a number of times equal to that index (2): ecabd.
    x rotate based on position of letter b finds the index of letter b (1), then rotates the string right once plus a number of times equal to that index (2): ecabd.
    rotate based on position of letter d finds the index of letter d (4),

    """
    assert rotate_based_on_char('abdec', 'b') == 'ecabd'
    assert rotate_based_on_char('ecabd', 'd') == 'decab'


def test_reverse():
    """
    swap letter d with letter b swaps the positions of d and b: edcba.
    reverse positions 0 through 4 causes the entire string to be reversed, producing abcde.

    bob=astring[start,end]
    foo=''.join (x for x in reversed(bob) )

    final=astring[0:start]+ foo + astring[end:]
    """
    val = reverse('edcba', 0, 4)
    assert val == 'abcde'


def test_move_position():
    """
    x rotate left 1 step shifts all letters left one position, causing the first letter to wrap to the end of the string: bcdea.
    x move position 1 to position 4 removes the letter at position 1 (c), then inserts it at position 4 (the end of the string): bdeac.
    move position 3 to position 0 removes the letter at position 3 (a), then inserts it at position 0 (the front of the string): abdec.

    """

    val = move_position('bcdea', 1, 4)
    assert val == 'bdeac'

    val = move_position(val, 3, 0)
    assert val == 'abdec'
