#!/usr/bin/env python
# -*- coding: utf-8 -*-

"""
File: twentyone.py
Author: yourname
Email: yourname@email.com
Github: https://github.com/yourname
Description:
"""

from collections import deque

# logging
import logging
logging.basicConfig(format='%(asctime)s:%(levelname)s:%(name)s:%(message)s')
logger = logging.getLogger(__name__)
logger.setLevel(logging.DEBUG)

def swap_index(astring, a, b):
    """
    """
    one = astring[a]
    two = astring[b]

    alist = list(astring)
    alist[b] = one
    alist[a] = two

    return ''.join(alist)


def swap_letters(astring, x, y):
    """ we cheat, since "X" is not in our set of
    starting letters
    """
    return astring.replace(x, "X").replace(y, x).replace("X", y)


def rotate_string(astring, how, number):
    cue = deque(astring)

    if how == 'right':
        cue.rotate(number)

    if how == 'left':
        cue.rotate(-number)

    return ''.join(list(cue))


def rotate_based_on_char(astring, char):
    """ huh? """
    location = astring.find(char)

    cue = deque(astring)
    cue.rotate(1)
    cue.rotate(location)
    if location >= 4:
        cue.rotate(1)

    return ''.join(list(cue))


def reverse(astring, start, end):
    bob = astring[start:end+1]
    foo = ''.join(x for x in reversed(bob))

    final = astring[0:start] + foo + astring[end+1:]
    return final


def move_position(astring, x, y):
    alist = list(astring)
    letter = alist[x]

    alist.pop(x)
    alist.insert(y, letter)
    return ''.join(alist)

if __name__ == '__main__':
    bob=rotate_based_on_char('ecabd', 'd') #== 'decab'
    print bob
