#!/usr/bin/env python
# -*- coding: utf-8 -*-

"""
File: parser.py
Author: yourname
Email: yourname@email.com
Github: https://github.com/yourname
Description:
"""

"""
    swap position X with position Y
    swap letter X with letter Y
    rotate left/right X steps
    rotate based on position of letter X
    reverse positions X through Y
    move position X to position Y
"""
import itertools
from tqdm import tqdm

from twentyone import (
    swap_index,
    swap_letters,
    rotate_string,
    rotate_based_on_char,
    reverse,
    move_position,
)

# logging
import logging
logging.basicConfig(format='%(asctime)s:%(levelname)s:%(name)s:%(message)s')
logger = logging.getLogger(__name__)
logger.setLevel(logging.INFO)


def rotate(line, starting=''):
    """
    rotate left/right X steps
    rotate based on position of letter X
    """
    if 'position' not in line:
        out = rotate_steps(line, starting)
    if 'position' in line:
        out = rotate_pos(line, starting)
    return out


def rotate_pos(line, starting=''):
    temp = line.split()
    letter = temp[-1]

    if starting:
        return rotate_based_on_char(starting, letter)

    if not starting:
        return letter


def rotate_steps(line, starting=''):
    temp = line.split()
    how = temp[1]
    number = int(temp[2])
    if starting:
        return rotate_string(starting, how, number)

    if not starting:
        return how, number


def rev(line, starting=''):
    """
    reverse positions X through Y
    """
    temp = line.split()
    start = int(temp[2])
    end = int(temp[-1])

    if starting:
        return reverse(starting, start, end)

    if not starting:
        return start, end


def move(line, starting=''):
    """
    move position X to position Y
    """
    temp = line.split()
    x = int(temp[2])
    y = int(temp[-1])

    if starting:
        return move_position(starting, x, y)

    if not starting:
        return x, y


def cleanup(line, words):
    line = line.strip()
    for word in words:
        line = line.replace(word, '')
    return line


def swap_pos(line, starting=''):
    """
    swap position X with position Y
    """
    line = cleanup(line, ('position', 'with', 'swap')).strip()
    temp = line.split()

    x = int(temp[0])
    y = int(temp[1])

    if starting:
        return swap_index(starting, x, y)

    if not starting:
        return x, y


def swap_letter(line, starting=''):
    """
    swap letter X with letter Y
    """
    line = cleanup(line, ('letter', 'with', 'swap')).strip()
    temp = line.split()

    x = temp[0]
    y = temp[1]

    if starting:
        return swap_letters(starting, x, y)

    if not starting:
        return x, y


def swap(line, starting=''):
    """
    swap letter X with letter Y
    """

    if 'position' in line:
        out = swap_pos(line, starting)

    if 'letter' in line:
        out = swap_letter(line, starting)

    return out


def parse_line(starting, line):
    line = line.strip()
    if line.startswith('swap'):
        out = swap(line, starting)

    if line.startswith('rotate'):
        out = rotate(line, starting)

    if line.startswith('reverse'):
        out = rev(line, starting)

    if line.startswith('move'):
        out = move(line, starting)

    return out


def parse_lines(starting, lines):
    for line in lines:
        before = starting
        starting = parse_line(starting, line)

        msg = '%s %s, %s' % (before, starting, line)
        logger.debug(msg)

    return starting


def part1(starting='abcdefgh'):
    lines = open('twentyone_data.txt', 'r').readlines()
    foo = parse_lines(starting, lines)
    return starting, foo


def part2(given):
    lines = open('twentyone_data.txt', 'r').readlines()

    combos = itertools.permutations(given, len(given) )

    """ just make a pretty tqdm 40320 = len """
    combos=[x for x in combos]

    for val in tqdm(combos):
        res = parse_lines(''.join(val), lines)
        if res == given:
            return ''.join(val)

    return None


def _test():
    text = """
swap position 4 with position 0
swap letter d with letter b
reverse positions 0 through 4
rotate left 1 step
move position 1 to position 4
move position 3 to position 0
rotate based on position of letter b
rotate based on position of letter d
    """
    starting = 'abcde'
    lines = [x.strip() for x in text.split('\n') if x.strip()]
    foo = parse_lines(starting, lines)
    return starting, foo


if __name__ == '__main__':
    # """ part1 """
    starting, end = part1()
    msg = 'Part 1: Starting :%s, ending:%s' % (starting, end)
    logger.info(msg)

    """ part2 """
    given='fbgdceah'
    res = part2(given=given)
    msg = 'Part 2: Starting :%s, given:%s' % (res, given)
    logger.info(msg)
