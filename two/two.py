#!/usr/bin/env python
# -*- coding: utf-8 -*-

"""
File: two.py
Author: jwhisnant
Email: jwhisnant@email.com
Github: https://bitbucket.org/jwhisnant
Description:
"""

import itertools

# logging
import logging
logging.basicConfig(format='%(asctime)s:%(levelname)s:%(name)s:%(message)s')
logger = logging.getLogger(__name__)
logger.setLevel(logging.INFO)
#logger.setLevel(logging.DEBUG)


class IllegalMove(Exception):

    """
    You cannot chose this button. They do not "wrap".
    """


class Keypad(object):

    def __init__(self,
                 size=3,
                 instructions=None,
                 position=(1, 1),
                 valid=(
                 (0, 0), (0, 1), (0, 2),
                 (1, 0), (1, 1), (1, 2),
                 (2, 0), (2, 1), (2, 2),
                 ),
                 seq='123456789',
                 ):
        """
        size - assume a square. you can find it next to the spherical cow
        instructions is an iterable of strings of ULDR

        """
        self.size = size
        self.seq = seq # optional number to other converter

        self.instructions = instructions
        if not self.instructions:
            self.instructions = ()

        self.position = position

        self.valid = valid

        self.traversal = []
        self.presses = []

        self.adict = {}
        self._setup()

    def _setup(self):

        for num, loc in enumerate(self.valid):
            self.adict[loc] = self.seq[num]

            self.direction_lookup = {'U': (-1, 0), 'D': (1, 0), 'L': (0, -1), 'R': (0, 1)}

    def __call__(self):
        """TODO: Docstring for __call__.
        Returns
        -------
        TODO

        """
        for astring in self.instructions:
            for direction in astring:
                move = self._move(direction)
                self.traversal.append((direction, move, self.adict.get(move, None)))

            self.presses.append(self.adict.get(move))

        return self

    def add(self, one, other):
        """ traverse buttons """
        candidate = (one[0] + other[0], one[1] + other[1])

        if candidate not in self.valid:
        # if candidate[0] not in (0, 1, 2) or candidate[1] not in (0, 1, 2):
            raise IllegalMove('Illegal move from: %s to %s' % (one, other))

        return candidate

    def _move(self, direction):
        diff = self.direction_lookup.get(direction, None)
        try:
            move = self.add(self.position, diff)
            self.position = move

        except IllegalMove as err:
            logger.debug(err)
            #self.position=self.position
            move = self.position

        return move


def read_file(fqn='two_data.txt'):
    return [x.strip() for x in open(fqn, 'r')]


def part1():
    instructions = read_file()
    keypad = Keypad(size=3, instructions=instructions, position=(1, 1))
    worked = keypad()
    logger.info('Part 1 %s' % (worked.presses))
    return keypad


def part2():
    instructions = read_file()

    valid = (
        (0, 2),
        (1, 1), (1, 2), (1, 3),
        (2, 0), (2, 1), (2, 2), (2, 3), (2, 4),
        (3, 1), (3, 2), (3, 3),
        (4, 2),
    )

    seq = '123456789ABCD'

    keypad = Keypad(size=5,
                    instructions=instructions,
                    position=(2, 0),
                    valid=valid,
                    seq=seq,
                    )

    worked = keypad()

    logger.info(worked.presses)

if __name__ == '__main__':
    #part1()
    part2()
