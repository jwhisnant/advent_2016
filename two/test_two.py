#!/usr/bin/env python
# -*- coding: utf-8 -*-

"""
File: test_two.py
Author: jwhisnant
Email: jwhisnant@email.com
Github: https://github.com/jwhisnant
Description:
"""

from two import Keypad

instructions = (
    'ULL',
    'RRDDD',
    'LURDL',
    'UUUUD',
)


def test_adict():
    """
    test basic structure
    """
    keypad = Keypad()
    assert keypad.adict.get((0, 0)) == '1'
    assert keypad.adict.get((1, 0)) == '4'
    assert keypad.adict.get((1, 1)) == '5'
    assert keypad.adict.get((2, 2)) == '9'


def test_add():
    """
            'U': (-1, 0),
            'D': (1, 0),
            'L': (0, -1),
            'R': (0, 1)}
    """

    keypad = Keypad()

    #U
    assert keypad.add((1, 1), (-1, 0)) == (0, 1)
    #L
    assert keypad.add((1, 1), (0, -1)) == (1, 0)
    #R
    assert keypad.add((1, 1), (0, 1)) == (1, 2)
    #D
    assert keypad.add((1, 1), (0, -1)) == (1, 0)


def test_case_part1():
    keypad = Keypad(instructions=instructions, position=(1, 1), size=3)
    worked = keypad()
    assert worked.presses == ['1', '9', '8', '5']


def test_case_part2():
    """ 5DB3 """
    valid = (
        (0, 2),
        (1, 1), (1, 2), (1, 3),
        (2, 0), (2, 1), (2, 2), (2, 3), (2, 4),
        (3, 1), (3, 2), (3, 3),
        (4, 2),
    )

    seq = '123456789ABCD'

    keypad = Keypad(size=5,
                    instructions=instructions,
                    position=(2, 0),
                    valid=valid,
        seq=seq,
                    )

    worked = keypad()
    assert worked.presses == ['5', 'D', 'B', '3']
