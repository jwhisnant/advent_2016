#!/usr/bin/env python
# -*- coding: utf-8 -*-

"""
File: test_dec2.py
Author: yourname
Email: yourname@email.com
Github: https://github.com/yourname
Description:
"""

from dec2 import process, expand, read


def test_process():
    data = (
        ('(3x3)XYZ', 9, 'XYZXYZXYZ'),
        ('X(8x2)(3x3)ABCY', 20, 'XABCABCABCABCABCABCY'),
        ('(27x12)(20x12)(13x14)(7x10)(1x12)A', 241920, 'A' * 241920),
        ('(25x3)(3x3)ABC(2x3)XY(5x2)PQRSTX(18x9)(3x2)TWO(5x7)SEVEN', 445, None),
    )

    for thing, expected_len, expected_out in data:
        _, length, recorded = process(thing, count=0, record=True)

        assert length == expected_len
        if expected_out:
            assert recorded == expected_out


def test_expand():
    assert expand('3x3', 'XYZ') == (3, 'XYZ' * 3)


def test_read():
    record = False
    assert read('ABC(3x2)YZ', 0) == ('(3x2)YZ', 3, '')
    assert read('(2x3)YZ', 3) == ('YZYZYZ', 3, '')
    assert read('YZ', 5) == ('', 7, '')

    record = True
    assert read('ABC(3x2)YZ', 0, True) == ('(3x2)YZ', 3, 'ABC')
    assert read('(2x3)YZ', 3) == ('YZYZYZ', 3, '')
    assert read('YZ', 5, True) == ('', 7, 'YZ')
