#!/usr/bin/env python
# -*- coding: utf-8 -*-

"""
File: dec2.py
Author: yourname
Email: yourname@email.com
Github: https://github.com/yourname
Description:
"""
from tqdm import tqdm
from collections import deque

# logging
import logging
logging.basicConfig(format='%(asctime)s:%(levelname)s:%(name)s:%(message)s')
logger = logging.getLogger(__name__)
logger.setLevel(logging.DEBUG)


def expand(instruction, later):
    temp = instruction.split('x')

    length = int(temp[0])
    rep = int(temp[1])

    return length, later[0:length] * rep


def read(queue, count, record=False):
    recorded = ''

    before, marker, after = queue.partition('(')

    instruction = None

    if before:
        count = count + len(before)

        """ record for small values to test algo """
        if record:
            recorded = queue[:len(before)]

        queue = queue[len(before):]

        return queue, count, recorded

    if not before:
        pos = after.find(')')

        instruction = after[0:pos]
        later = after[pos + 1:]

        if instruction:
            length, stuff = expand(instruction, later)

            if stuff:

                """ doc remove up to length from queue """
                queue = later[length:]

                """ add the stuff we expanded to the front """
                queue = stuff + queue

                return queue, count, recorded

    return queue, count, recorded


def process(data, count, record=False):
    recorded = ''
    count = 0

    for num, char in enumerate(data):
        if char == '(':
            start = num

    start = data.count('(')

    with tqdm() as pbar:
        while data:
            pbar.update(); count = count + 1

            data, count, incremental = read(data, count, record)

            if incremental:
                recorded = recorded + incremental

            if count % 100000 == 0:
                tqdm.write('(%s) %s' % (count, data[:200]))

    return data, count, recorded


def part2(record=False):
    data = ''.join([x.strip() for x in open('nine_data.txt', 'r').readlines()] )
    #data = deque(data)

    """ too big to record, not enough memory """
    _, length, recorded = process(data, count=0, record=record)

    return length

if __name__ == '__main__':
    length = part2(record=True)

    msg = 'Length unencrypted data: %s' % (length)
    logger.info(msg)

    #too high
    # 296744732it [3:54:24, 21098.88it/s]
    # 2016-12-19 02:37:23,939:INFO:__main__:Length unencrypted data: 11421771560
