#!/usr/bin/env python
# -*- coding: utf-8 -*-

"""
File: parser.py
Author: yourname
Email: yourname@email.com
Github: https://github.com/yourname
Description:
"""

# logging
import logging
logging.basicConfig(format='%(asctime)s:%(levelname)s:%(name)s:%(message)s')
logger = logging.getLogger(__name__)
logger.setLevel(logging.DEBUG)


class Parser(object):

    """Docstring for Parser. """

    def __init__(self,
                 data,
                 left=0,
                 right=0,
                 next_left=0,
                 pos=0,
                 remaining=None,
                 ):

        self._data = data
        self.data = data

        self.count = 0

        self.left = left
        self.right = right
        self.next_left = next_left
        self.pos = pos
        self.remaining = remaining
        self.done = False

        self.fragments = {}

    def __call__(self):
        """TODO: Docstring for __call__.
        Returns
        -------
        TODO

        """
        self.read()
        return self

    def str2math(self, frag):
        """TODO: Docstring for str2math.

        Parameters
        ----------
        frag : string

        Returns
        -------
        length

        """
        if frag.count('(') != 1:
            raise ValueError('Too many parens')

        if not frag.startswith('('):
            raise ValueError('Too many parens')

        loc = frag.find(')') + 1
        length, rep = self.get_length(frag[:loc])

        chars = frag[loc:]

        if len(chars) == length:
            return length * rep

        if len(chars) > length:
            return length * rep + len(chars) - length

        if len(chars) < length:
            raise ValueError('Too short length')

    def update_fragments(self, frag):

        self.fragments[self.count] = frag
        msg = '%s %s' % (self.count, self.fragments.get(self.count))
        logger.info(msg)

        self.count = self.count + 1

    def get_length(self, data):
        data = data.replace('(', '').replace(')', '')

        temp = data.split('x')
        length = int(temp[0])
        rep = int(temp[1])
        return length, rep

    def update_frags(self, frag, before, after, length, repeat, out=None):

        if not out:
            out = {}

        if frag not in out:
            out[frag] = {}

            out[frag]['before'] = before
            out[frag]['after'] = after
            out[frag]['length'] = length
            out[frag]['repeat'] = repeat

        return out

    def expand_frags(self, frag):
        befores = []
        afters = []

        before = ''
        after = ''
        length = len(frag)
        repeat = 1

        out = {'before': [], 'after': []}
        # out = self.update_frags(frag, before, after, length, repeat, out)

        while frag:
            out = self.update_frags(frag, before, after, length, repeat, out)
            before, after, next_frag, length, repeat = self.expand(frag)

            if before:
                befores.append(before)

            if after:
                afters.append(after)

            if frag != next_frag:
                frag = next_frag

            """we are done with this piece, do "after" next """
            if frag == next_frag:
                frag = after

        return out

    def expand(self, frag):
        """
        There are no characters "before" that is
        a frag must begin with a '('
        return length, repeat, next_frag , after
        """

        if frag.count('(') == 1:
            return frag

        before = frag[:frag.find('(')]

        loc = frag.find(')') + 1
        length, repeat = self.get_length(frag[:loc])

        after = frag[loc:]

        next_frag, after = self.found_left(frag[loc:])
        return before, after, next_frag, length, repeat

    def read(self):
        fragment = ''

        data = self.data[self.pos:]

        while data:

            char = data[0]

            if char != '(':
                fragment = fragment + char

                """ previous data """
                self.update_fragments(fragment)
                data = data[1:]

            if char == '(':
                fragment, data = self.found_left(data)
                """ new data """
                self.update_fragments(fragment)

    def found_left(self, data, pos=0):
        fragment = ''

        left = data.find('(')
        right = data.find(')')

        length, rep = self.get_length(data[left:right + 1])

        how_far = right + 1 + length
        fragment = data[pos:right + 1 + length]
        remaining = data[how_far:]

        return fragment, remaining


if __name__ == '__main__':

    # frag='(6x9)JUORKH'
    # print expand(frag)

    frag = ''.join([x.strip() for x in open('14.txt', 'r').readlines()])
    parser = Parser(data=frag)
    # parser.expand(frag)
    parser.expand_frags(frag)

    # data = ''.join([x.strip() for x in open('nine_data.txt', 'r').readlines()])
    parser = Parser(data=data)
    worked = parser()

    for num, frag in sorted(worked.fragments.items()):
        print frag, worked.str2math(frag)
