#!/usr/bin/env python
# -*- coding: utf-8 -*-

"""
File: fragment.py
Author: yourname
Email: yourname@email.com
Github: https://github.com/yourname
Description:
    A fragment class and manager for problem 9
    fortunately there are some shortcuts since
    these are not in our "data" : 'x','(',')', and ints
"""

from pprint import pprint as pp

import attr

import operator
import functools

# logging
import logging
logging.basicConfig(format='%(asctime)s:%(levelname)s:%(name)s:%(message)s')
logger = logging.getLogger(__name__)
logger.setLevel(logging.INFO)


@attr.s
class Fragment(object):

    """Docstring for Fragment. """
    before = attr.ib(default='')
    after = attr.ib(default='')
    chars = attr.ib(default='')

    next_fragment = attr.ib(default=attr.Factory(list))
    numbers = attr.ib(default=attr.Factory(list))
    size = attr.ib(default=0)

    reps = attr.ib(default=0)
    num = attr.ib(default=0)
    level = attr.ib(default=0)

    def __call__(self):
        """ doc """
        self.set_before()
        self.update()
        return self

    def update(self):
        res = self.num_reps()
        self.num, self.reps, self.next_fragment, self.after = res

        msg = 'num:%s reps:%s next_frag:%s' % (self.num,
                                               self.reps,
                                               self.next_fragment)

        logger.debug(msg)
        # self.numbers.append(self.reps)
        return res

    def set_before(self):
        """ find any characters before a starting ( """
        self.before = self.chars[0:self.chars.find('(')]

    def num_reps(self):
        """ calculate the
        first number and reps
        and
        """
        start = self.chars.find('(')
        end = self.chars.find(')')

        """ if there is no start
        marker, then we can just return that
        there is one number and rep
        """
        if start == -1:
            num = 1
            reps = 1
            next_frag = ''
            after = ''

        """ if there is ( in the data """
        if start != -1:
            ins = self.chars[start + 1:end]

            temp = ins.split('x')
            num = int(temp[0])
            reps = int(temp[1])

            end_frag = end + 1 + num
            next_frag = self.chars[end + 1:end_frag]
            after = self.chars[end_frag:]

        return num, reps, next_frag, after


@attr.s
class FragmentManager(object):

    """Docstring for FragmentManager. """

    text = attr.ib(default='')

    length = attr.ib(default=0)
    expansions = attr.ib(default=0)
    level = attr.ib(default=0)
    rounds = attr.ib(default=0)

    numbers = attr.ib(default=attr.Factory(list))
    pending = attr.ib(default=attr.Factory(list))

    def expand(self, worked):
        """ handle if we only have characters left,
        if so, then we can calculate a length and add it
        """
        nums = worked.numbers + [len(worked.next_fragment)] + [worked.reps]
        size = functools.reduce(operator.mul, nums, 1)
        self.length = self.length + size
        self.expansions = self.expansions + 1

        self.text = worked.after

    def dont_expand(self, worked):
        """ if there are more expansions in the
        the next fragment, then we cant get a size
        yet. Put things onto a pending queue to analyze later.
        """

        self.text = worked.next_fragment
        self.level = worked.level + 1

        numbers = worked.numbers + [worked.reps]
        self.pending.append((worked.numbers, worked.after))
        self.numbers = numbers

    def process(self, worked):
        """We process a fragment """

        """ we can always add the before stuff since its simple chars"""
        self.length = len(worked.before) + self.length

        """ we can add the after if not ( since it is just chars """
        if '(' not in worked.after:
            self.length = len(worked.after) + self.length

        """ we can finish a fragment and expand it"""
        if '(' not in worked.next_fragment:
            self.expand(worked)

        """ a fragment is not finished so we can setup for later"""
        if '(' in worked.next_fragment:
            self.dont_expand(worked)

    def check(self):
        if not self.text and not self.pending:
            return False

        return True

        if self.text:
            return True

        if not self.text:
            if self.pending:
                return True

    def __call__(self):
        before = ''
        after = ''

        while self.check():
            fragment = Fragment(chars=self.text,
                                before=before,
                                after=after,
                                numbers=self.numbers,
                                )
            worked = fragment()
            self.rounds = self.rounds + 1

            self.process(worked)

            """ if we have run out of text, there might be more in
            the pending list, if so, pop some data and keep going
            """

            if not self.text:
                if self.pending:
                    stuff = self.pending[0]
                    msg = 'Len next: %s, some:%s' % (len(stuff[1]),
                                                     stuff[1][:100])

                    logger.debug(msg)

                    """ advance """
                    self.pending = self.pending[1:]

                    self.numbers = stuff[0]
                    self.text = stuff[1]

        return self

def manage(text):
    manager = FragmentManager(text=text)
    worked = manager()
    return worked


if __name__ == '__main__':
    text = ''.join([x.strip() for x in open('nine_data.txt', 'r').readlines()])
    managed = manage(text)

    msg = 'Compressed length:%s, uncompressed_length:%s, Sample:%s' % (len(text),
                                                                       managed.length,
                                                                       repr(text[:100])
                                                                       )
    logger.info(msg)

    #correct
    "Compressed length:10995, uncompressed_length:11125026826, Sample:'(6x9)JUORKH...' "
