#!/usr/bin/env python
# -*- coding: utf-8 -*-

"""
File: nine.py
Author: yourname
Email: yourname@email.com
Github: https://github.com/yourname
Description:
"""

import re
import string

# logging
import logging
logging.basicConfig(format='%(asctime)s:%(levelname)s:%(name)s:%(message)s')
logger = logging.getLogger(__name__)
logger.setLevel(logging.DEBUG)


class Decompressor(object):

    """Docstring for V1 Decompressor. """

    def __init__(self,
                 out=None,
                 data=None,
                 fqn='nine_data.txt',
                 ):

        self.out = out
        self.fqn = fqn
        self.data = data

        self.expansions = 0
        self.frames = []

        self.processed = ''

        if self.fqn and not self.data:
            self.data = self.read_file()

    def __repr__(self):
        return '<%s: exp:%s>' % (self.__class__.__name__,
                                 self.expansions,
                                 )

    def __call__(self):
        if '(' not in self.data:
            """ set processed and length"""
            self.processed=self.data
            self.length = self.len()

            msg = "%s has no '(' nothing to do ..." % (repr(self))
            logger.warning(msg)
            return self

        adict = {'pos': 0}

        data = self.data

        while data:
            adict = self.scan(data)

            self.processed = self.processed + adict['before']
            data = adict['after']

        self.length = self.len()
        return self

    def len(self):
        """ len, no whitespace """
        out = ''
        for char in self.processed:
            if char not in string.whitespace:
                out = out + char
        return len(out)

    def scan(self, astring, adict=None):
        """
        return adict of parsed info
        """

        expansion = None
        where = 'before'
        num = None

        if not adict:
            adict = {
                'before': '',
                'frame': '',
                'after': '',
                'pos': None,
            }

        for pos, char in enumerate(astring):

            if char == '(' and not adict.get('frame'):
                where = 'frame'

            if char == ')':
                adict[where] = adict[where] + char
                adict['after'] = astring[pos + 1:]

            if not adict['after']:
                adict[where] = adict[where] + char

            if adict['after']:
                num, rep = self.check_algo(adict['frame'])

                if num and rep:

                    expansion = self.decompress(num, rep,
                                                adict.get('after'))
                    self.frames.append((adict['frame'],
                                        adict.get('after')[:num],
                                        expansion))

            if expansion:
                self.expansions = self.expansions + 1
                adict['before'] = adict['before'] + expansion

                # XXX TODO: important 2016/12/14  (james)
                adict['after'] = adict['after'][num:]

                adict['pos'] = pos + num + 1
                return adict

        if not expansion:
            adict['before'] = adict['before'] + adict['frame']

        adict['pos'] = pos
        return adict

    def decompress(self, num, rep, some_string):
        expansion = some_string[0:num] * rep
        return expansion

    def check_algo(self, frame):
        try:
            num, rep = frame[1:-1].split('x')
        except:
            import ipdb; ipdb.set_trace()  #XXX BREAKPOINT
            num, rep = frame[1:-1].split('x')
            return None, None

        return int(num), int(rep)

    def read_file(self):
        # XXX TODO: for small enough files 2016/12/13  (james)
        data = ''.join([x.strip() for x in open(self.fqn, 'r').readlines()] )
        return data


def version2(dec):
    """
    dec is a configured dec instance
    with a starting state
    """
    starting = dec.data
    workers = []
    worked = dec()
    processed = worked.processed

    while len(worked.data) != len(worked.processed):
    # XXX FIXME: while input!=output 2016/12/14  (jwhisnant)
    # for x in range(1, 10):
        new_worker = Decompressor()
        new_worker.data = worked.processed
        worked = new_worker()

        msg = '%s : len incoming data:%s, len processed:%s' % (
            repr(worked),
            len(worked.data),
            len(worked.processed),
        )

        logger.info(msg)
        workers.append(worked)

    return workers


def part1():
    dec = Decompressor()
    worked = dec()

    msg = 'Decompressed length : %s' % (worked.length)
    logger.info(msg)

    #138735 - woot - right first time


def part2():
    dec = Decompressor()
    stuff = version2(dec)

    last_worker = stuff[-1]
    msg = 'Decompressed length : %s' % (last_worker.length)
    logger.info(msg)

if __name__ == '__main__':
    part1()
    # part2()
