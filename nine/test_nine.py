#!/usr/bin/env python
# -*- coding: utf-8 -*-

"""
File: nine_test.py
Author: yourname
Email: yourname@email.com
Github: https://github.com/yourname
Description:
"""

from nine import Decompressor
from nine import version2


def test_decompressor_v1():
    """TODO: Docstring for test_no_decompress.

    Parameters
    ----------

    Returns
    -------
    TODO

    """
    data = (
        ('ADVENT', 'ADVENT', 6),
        ('A(1x5)BC', 'ABBBBBC', 7),
        ('(3x3)XYZ', 'XYZXYZXYZ', 9),
        ('A(2x2)BCD(2x2)EFG', 'ABCBCDEFEFG', 11),
        ('(6x1)(1x3)A', '(1x3)A', 6),
        ('X(8x2)(3x3)ABCY', 'X(3x3)ABC(3x3)ABCY', 18),
    )

    for start, finish, length in data:
        dec = Decompressor(data=start)
        worked = dec()

        assert worked.processed == finish
        assert worked.length == length


def test_decompressor_v2():
    """TODO: Docstring for test_no_decompress.

    Parameters
    ----------

    Returns
    -------
    TODO

    """
    data = (
        ('(3x3)XYZ', 'XYZXYZXYZ', 9),
        ('X(8x2)(3x3)ABCY', 'XABCABCABCABCABCABCY', len('XABCABCABCABCABCABCY')),
        ('(27x12)(20x12)(13x14)(7x10)(1x12)A', 'A'*241920, 241920),
        ('(25x3)(3x3)ABC(2x3)XY(5x2)PQRSTX(18x9)(3x2)TWO(5x7)SEVEN', None, 445),
    )

    for start, finish, length in data:
        dec = Decompressor(data=start)
        workers = version2(dec)
        last_worker = workers[-1]

        if finish:
            assert last_worker.processed == finish

        assert last_worker.length == length
