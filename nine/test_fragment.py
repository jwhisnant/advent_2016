#!/usr/bin/env python
# -*- coding: utf-8 -*-

"""
File: test_fragment.py
Author: yourname
Email: yourname@email.com
Github: https://github.com/yourname
Description:
    Test a fragment class and manager for problem 9
    fortunately there are some shortcuts since
    these are not in our "data" : 'x','(',')', and ints
"""

from fragment import Fragment, FragmentManager

def test_manager():
    data = (
        ('(3x3)XYZ', 9, 'XYZXYZXYZ'),
        ('X(8x2)(3x3)ABCY', 20, 'XABCABCABCABCABCABCY'),
        ('(27x12)(20x12)(13x14)(7x10)(1x12)A', 241920, 'A' * 241920),
        ('(25x3)(3x3)ABC(2x3)XY(5x2)PQRSTX(18x9)(3x2)TWO(5x7)SEVEN', 445, None),
    )

    for text, expected_len, expected_out in data:
        manager = FragmentManager(text=text)
        worked = manager()
        assert(worked.length)==expected_len
