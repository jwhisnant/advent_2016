#!/usr/bin/env python
# -*- coding: utf-8 -*-

"""
doc
"""

"""
File: six.py
Author: yourname
Email: yourname@email.com
Github: https://github.com/yourname
Description:
"""

from collections import Counter

# logging
import logging
logging.basicConfig(format='%(asctime)s:%(levelname)s:%(name)s:%(message)s')
logger = logging.getLogger(__name__)
logger.setLevel(logging.DEBUG)


def most_common(alist, option):
    out = []
    transpose = zip(*alist)

    for thing in transpose:

        if option == 'most':
            val = Counter(thing).most_common(1)
            out.append(val[0][0])

        if option == 'least':
            val = Counter(thing).most_common()[-1]
            out.append(val[0][0])

    return ''.join(out)

if __name__ == '__main__':

    data = open('sex_data.txt', 'r').read()

    alist = data.split()
    foo = most_common(alist, option='most')
    logger.info('most %s' % (foo))

    alist = data.split()
    foo = most_common(alist, option='least')
    logger.info('least %s' % (foo))
