#!/usr/bin/env python
# -*- coding: utf-8 -*-

"""
File: test_six.py
Author: yourname
Email: yourname@email.com
Github: https://github.com/yourname
Description:
"""

from sex import most_common

test_string = """
eedadn
drvtee
eandsr
raavrd
atevrs
tsrnev
sdttsa
rasrtv
nssdts
ntnada
svetve
tesnvt
vntsnd
vrdear
dvrsen
enarar
"""


def test_most_common():
    alist = test_string.split()
    assert most_common(alist, option='most') == 'easter'


def test_least_common():
    alist = test_string.split()
    assert most_common(alist, option='least') == 'advent'


if __name__ == '__main__':
    test_most_common()
